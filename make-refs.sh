cd overleaf
for f in $(find standalones/ -name '*.tex'); 
do
    latexmk -pdf $f;
    if [ $? -eq 0 ]
    then #            ./../make-refs.sh $f
        mkdir -p pdf/$(dirname $f);
        echo $(dirname $f);
        mv $(basename $f .tex).pdf pdf/$(dirname $f);
        mv $f pdf/$(dirname $f);
        cat  << ENDOF >> pdf/$(dirname $f)/index.html
 <!DOCTYPE html>
 <html>
  <head>
      <title>$(basename $f .tex)</title>
      <link  href=".././stylesheets/stylesheet.css" media="screen" type="text/css" rel="stylesheet"/>
  </head>
  <body>
      <p>$(basename $f .tex): le <a href="./$(basename $f .tex).pdf">document</a>, et sa <a href="./$(basename $f .tex).tex">source</a>.</p>
  </body>
  </html>
ENDOF
    else
        echo 'Error compiling $f: exiting with status code 1';
        exit 1;
    fi
done;
cd ..