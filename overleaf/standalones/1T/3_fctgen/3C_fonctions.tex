\documentclass[a4paper,twocolumn, landscape]{article}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc} 
\usepackage[french]{babel}
\usepackage{amsmath,amssymb}
\usepackage{geometry}
\geometry{top=1cm, bottom=1cm, left=1cm, right=1cm}
\setlength\columnsep{2cm}
\usepackage{tikz, tkz-tab}
\usepackage{fourier}
\usepackage{comment}
\usepackage{codesnippet}

\newcommand\defin{\paragraph{Définition.}}
\newcommand\thm[1][.]{\paragraph{Théorème \emph{#1}}}
\newcommand\ex[1][.]{\paragraph{Exemple \emph{#1}}}
\newcommand\rem{\paragraph{Remarque.}}
\newcommand\notat[1][.]{\paragraph{Notation \emph{#1}}}
\newcommand\meth[1]{\paragraph{Méthode:~\emph{#1.}}}
\newcommand\prop[1][.]{\paragraph{Propriété \emph{#1}}}
\newcommand\demo{\paragraph{Démonstration.}}
\newcommand\demoroc{\paragraph{Démonstration ($\Delta$ROC).}}

\renewcommand\thesection{\Roman{section}.}
\renewcommand\thesubsection{\arabic{subsection})}
\renewcommand\thesubsubsection{\alph{subsubsection}}

\pagestyle{empty}

\newcount\nbcopy
\nbcopy=1

\begin{document}

\begin{DefineCodeSnippet}{contents}
%\hfil{\large\bfseries Comment représenter une fonction?}\hfill

\subsubsection{Représentation algébrique}
Une fonction peut être définie avec une expression littérale.

\begin{ex}
La fonction $f$ définie sur $\mathbb R$ par $x \mapsto x^2 - 2 x -2$ (on écrit plus souvent $f(x) = x^2 - 2 x-2$).
\end{ex}

\begin{rem}
    Plusieurs expressions peuvent désigner la même fonction. Par exemple, la fonction $f$ précédente admet aussi pour expression $f(x) = (x-1)^2-3$.
\end{rem}

\underline{Détermination d'images:} remplacer la valeur de la variable par le nombre souhaité, et réduire.

\underline{Détermination d'antécédents}: résoudre l'équation $f(x) = b$ donnera les antécédents de $b$.

\begin{ex}
L'image de $3$ est $f(3) = 3^2 - 2 \times 3 -2 = 9 - 6 -2 = 1$.

Recherchons les antécédents de $-2$:\\
on résout l'équation $f(x) = -2$:
\begin{gather*}
    x^2 - 2 x -2= -2\\
    x^2 - 2x = 0\\
    x(x - 2) = 0\\
    x = 0 \text{ ou } x-2 = 0;
\end{gather*}
les antécédents de $-2$ sont $0$ et $2$.
\end{ex}

\subsubsection{Représentation graphique}
\label{recherche_antecedents}

\def\graphicsize{4.2cm}
\noindent\begin{minipage}{\graphicsize}
\begin{tikzpicture}[scale=0.5]
\def\xa{-3.2}\def\xb{4.2}
\def\ya{-3.2}\def\yb{10.2}
\draw[gray, very thin]  (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=-2.6:4.2, samples=80, thick] plot(\x,{(\x-1)^2-3)});
\draw[dashed, ->, very thick] (-2,0) -- (-2,6) -- (0,6);
\draw[densely dotted, very thick] (\xa,1) -- (\xb, 1);
\foreach \x in {-1,3} \draw [->, dotted, very thick] (\x,1) -- (\x,0);
\end{tikzpicture}
\end{minipage}
\begin{minipage}{\dimexpr\linewidth-\graphicsize\relax}

\begin{meth}{Lire graphiquement une image}

Pour lire l'image d'un nombre réel $a$ d'une fonction dont on a la courbe représentative $\mathcal C$:
\begin{enumerate}
    \item repérer le point d'abscisse $a$ sur l'axe des abscisses;
    \item tracer le segment vertical de ce point vers la courbe $\mathcal C$;
    \item depuis ce point d'intersection, tracer le segment horizontal entre $\mathcal C$ et l'axe des ordonnées.
\end{enumerate}
\end{meth}

\begin{meth}{Lire graphiquement des antécédents éventuels}

Pour lire les éventuels antécédents d'un nombre réel $k$ d'une fonction dont on a la courbe représentative $\mathcal C$:
\begin{enumerate}
    \item repérer le point d'ordonnée $k$ sur l'axe des ordonnées;
    \item tracer la droite horizontale passant par ce point;
    \item pour \textbf{chaque} intersection de cette droite avec $\mathcal C$, tracer un segment vertical entre $\mathcal C$ et l'axe des abscisses;
    \item lire toutes les abscisses ainsi trouvées.
\end{enumerate}
\end{meth}
\end{minipage}


\underline{Lecture graphique d'images :} exemple avec les tirets : lecture de l'image de -2.\\
on trace la droite verticale à l'abscisse -2;
on lit l'ordonnée du point d'intersection de cette droite avec la courbe qui représente la fonction (ici: 6).

\underline{Lecture graphique d'antécédents :} exemple avec les pointillés : lecture des antécédents de 1.\\
on trace la droite horizontale à l'ordonnée 1;
on repère tous les points de la courbe qui intersectent cette droite,
on lit (par des droites  verticales) l'abscisse de chacun de ces points.\\
Ici, on lit pour abscisses $-1$ et $3$.\\
Les antécédents de $1$ par $f$ sont $-1$ et $3$.

\subsubsection{\`A l'aide d'un algorithme}
\begin{ex}
La fonction $f$ est définie à l'aide des instructions suivantes:

\noindent Choisir un nombre.\\
L'élever au carré.\\
Lui retirer son double.\\
Retirer 2 au résultat.\\
\end{ex}

\underline{Détermination d'images:} application des instructions.

\underline{Détermination d'antécédents:} si possible, en "inversant" les instructions; sinon, en résolvant une équation.

\subsubsection{Tableaux}
On peut obtenir, ou résumer, des informations sur les fonctions dans des tableaux.

\underline{Tableau de valeurs:} donne une correspondance entre des nombres et leurs images.

\begin{tabular}{*{11}{c|}}
    $x$ & $-2,5$ & $-2$ & $-1,5$ & $-1$ & $-0,5$ & $0$ & $0,5$ & $1$ & $1,5$ & $2$\\\hline
     $f(x)$ & $9,25$ & $6$ & $3,25$ & $ 1$ & $-0,75$ & $-2$ & $-2,75$ & $-3$ & $-2,75$ & $-2$
\end{tabular}
 \vspace{2ex}

\underline{Tableau de signes:} donne le signe d'une fonction entre deux valeurs.

\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , Signe $f(x)$ / 1}{$-3$, $-0.713$, $2.713$, $4$}
   \tkzTabLine{, +, z, -, z, +, }
\end{tikzpicture}

 \vspace{2ex}

\underline{Tableau de variations:} décrit le sens de variation de la fonction entre des valeurs.

\begin{tikzpicture}
   \tkzTabInit{$x$ / 1 , Var. $f(x)$ / 1}{$-3$, $1$, $4$}
    \tkzTabVar{+/ 13, -/ $-3$, +/ 6}

\end{tikzpicture}

\end{DefineCodeSnippet}

\loop
\ExecuteCodeSnippet{contents}\pagebreak
\advance \nbcopy by -1
\unless\ifnum \nbcopy<1 \repeat
\end{document}