\documentclass[a4paper,twocolumn, landscape]{article}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc} 
\usepackage[french]{babel}
\usepackage{amsmath,amssymb}
\usepackage{geometry}
\geometry{top=1.2cm, bottom=1.2cm, left=1cm, right=1cm}
\setlength\columnsep{2cm}
\usepackage{tikz, tkz-tab}
\usepackage{fourier}
\usepackage{comment}
\usepackage{codesnippet}

\newcommand\defin{\paragraph{Définition.}}
\newcommand\thm[1][.]{\paragraph{Théorème \emph{#1}}}
\newcommand\ex[1][.]{\paragraph{Exemple \emph{#1}}}
\newcommand\rem{\paragraph{Remarque.}}
\newcommand\notat[1][.]{\paragraph{Notation \emph{#1}}}
\newcommand\meth[1]{\paragraph{Méthode:~\emph{#1.}}}
\newcommand\prop[1][.]{\paragraph{Propriété \emph{#1}}}
\newcommand\demo{\paragraph{Démonstration.}}
\newcommand\demoroc{\paragraph{Démonstration ($\Delta$ROC).}}

\renewcommand\thesection{\Roman{section}.}
\renewcommand\thesubsection{\arabic{subsection})}
\renewcommand\thesubsubsection{\alph{subsubsection}}

\pagestyle{empty}

\newcount\nbcopy
\nbcopy=1

\begin{document}

\begin{DefineCodeSnippet}{contents}
\setcounter{subsection}1
\subsection{Résolution algébrique d'équations}



\begin{meth}{Résoudre des équations (polynomiales) par le calcul}
    Pour résoudre une équation de type $f(x) = k$:
    \begin{enumerate}
        \item on commence par écrire l'équation équivalente $f(x) - k = 0$;
        \item on factorise l'expression $f(x) - k$;
        \item on applique la règle du produit nul, puis on résout les mini-équations résultantes;
        \item on conclut en écrivant toutes les solutions obtenues.        
    \end{enumerate}
\end{meth}

\begin{rem}
    Pour l'étape de factorisation, soit il s'agit d'une factorisation simple, soit on utilise une identité remarquable, soit elle est guidée ou donnée (et on vérifie en développant que la factorisation est correcte).
\end{rem}

\bigskip

Pour rappel:\vspace{-3ex}
\begin{prop}
    Soit $a$, $b$ deux nombres réels. Alors on a les trois identités suivantes, appelées \emph{identités remarquables}:
    \begin{gather*}
        (a + b) ^2 = a^2 + 2ab + b^2\\
        (a - b) ^2 = a^2 - 2ab + b^2\\
        (a+b)(a-b) = a^2 - b^2
    \end{gather*}
\end{prop}

\begin{ex}
    Quels sont les antécédents éventuels de $3$ pour la fonction $f$ telle que $f(x) = x^2 - 2x +4$?\\
    On commence la résolution:
    \begin{equation*}
        f(x) = 3 \iff x^2 - 2x +4 = 3 \iff x^2 - 2x + 1 = 0.
    \end{equation*}
    De là, sans indication, il faut penser aux identités remarquables. La deuxième (avec $a = x$ et $b = 1$) fait le travail: comme $x^2 - 2x + 1 = (x - 1)^2$, l'équation devient $(x - 1)^2 = 0$. Par suite, un carré est nul si et seulement si le nombre de base l'est, donc $x - 1 = 0$, d'où $x = 1$.\\
    La fonction $f$ n'admet qu'un antécédent de 3, et cet antécédent est $1$.
\end{ex}
\subsection{Résolution d'inéquations du premier degré}
Pour rappel, les règles sur les inégalités sont les suivantes.
\begin{prop}
Dans toute inégalité:
\begin{itemize}
    \item le sens est \emph{conservé} si l'on ajoute ou retire une même quantité aux deux membres;
    \item le sens est \emph{conservé} si l'on multiplie ou l'on divise les deux membres par un nombre réel \emph{positif};
    \item le sens est \textbf{\emph{inversé}} si l'on multiplie  ou l'on divise les deux membres par un nombre réel \textbf{\emph{négatif}}.
\end{itemize}
\end{prop}
La propriété qui précède nous donne la base pour résoudre des inéquations du premier degré. La même méthode que celle des équations du premier degré s'applique, et nous la rappelons.

\begin{meth}{Résoudre des équations et des inéquations du premier degré}
La résolution d'équations et d'inéquations du premier degré passe par différentes étapes, chaque étape devant donner une équation ou une inéquation équivalente, plus simple à résoudre. Pour connaître l'opération à effectuer à chaque étape, il est bon de se poser les quatre questions suivantes:
\begin{enumerate}
    \item Qu'est-ce que je cherche? Quelle est l'inconnue?
    \item Qu'est-ce qui m'ennuie? Autrement dit: l'inconnue est englobée dans une expression, quels en sont les autres composants?
    \item Dans l'expression, quel est la dernière opération effectuée? Quel(s) "composant(s)" doi(ven)t être traité(s) en premier?
    \item Comment se débarasser de ce(s) "composant(s)"?
\end{enumerate}
C'est la réponse à cette dernière question qui donnera l'opération à effectuer.
\end{meth}

\begin{ex}
Résolvons dans $\mathbb R$ l'inéquation $3x - 1 \leq 5$:
\begin{align*}
    3x - 1  &\leq 5 &&\text{Membre de gauche, dernière opération:}-1\\
    3x - 1 {\color{blue}+1} &\leq 5{\color{blue}+1}\\
    3x &\leq 6 &&\text{Facteur 3 gênant: division}\\
    3x {\color{blue}/3} &\leq 6{\color{blue}/3}\\
    x &\leq 2
\end{align*}
L'ensemble des solutions est alors $S = ]-\infty; 2]$.
\end{ex}

\begin{ex}
Pour quels réels la fonction affine $f$ définie sur $[-3; 5]$ par $f(x) = -2x+4$, retourne-t-elle des images supérieures (strictement) à 1?\\
Il suffit d'abord de résoudre l'inéquation $-2x + 4 > 1$:
\begin{align*}
    -2x + 4 &> 1\\
    -2x &> 1 - 4 = -3\\
    x &< \frac{-3}{-2}&&\text{(l'inégalité change de sens car on a divisé par -2)}\\
    x &< 1,5.
\end{align*}
Mais le contexte nous impose aussi $x \geq -3$. Ainsi, l'ensemble des réels $x$ tels que $f(x) > 1$ est l'ensemble $[-3; 1,5[$.
\end{ex}

    
\end{DefineCodeSnippet}

\loop
\ExecuteCodeSnippet{contents}\pagebreak
\advance \nbcopy by -1
\unless\ifnum \nbcopy<1 \repeat
\end{document}

\begin{comment}


\subsection{Résolution d'inéquations polynomiales de degré plus grand que 1}
\subsubsection{Tableaux de signes}
Nous avons déjà étudié la notion de tableaux de signes, et avons présenté ceux qui conviennent pour les fonctions affines. Qu'en est-il pour des expressions (et donc, des fonctions) plus complexes?

Tout va être basé sur le résultat suivant, que l'on connaît déjà pour les nombres réels:
\begin{prop}
    Soit une expression $A$, produit de facteurs.
    \begin{itemize}
        \item S'il y a un nombre pair de facteurs négatifs, alors $A$ est positif.
        \item S'il y a un nombre impair de facteurs négatifs, alors $A$ est négatif.
        \item Si un facteur s'annule, alors $A$ est nul.
    \end{itemize}
\end{prop}

\'Ecrivons cette propriété mathématiquement avec deux facteurs:
\begin{prop}
    Soit $A(x)$ et $B(x)$ deux expressions littérales, et $r$ un nombre réel.
    \begin{itemize}
        \item Si $A(r)$ et $B(r)$ sont tous les deux positifs, ou tous les deux négatifs, alors $A(r)\times B(r)$ est positif.
        \item Si parmi $A(r)$ et $B(r)$, un seul des deux est négatif, alors $A(r)\times B(r)$ est négatif.
        \item Le produit $A(r)\times B(r)$ est nul si (et seulement si) l'un des deux au moins, parmi $A(r)$ et $B(r)$, est nul.
    \end{itemize}
\end{prop}

Grâce à cette propriété, et le fait que l'on détermine facilement le signe de fonctions affines, nous établissons une méthode pour connaître le signe d'une expression (factorisée) en fonction de la valeur que prend la variable.

\begin{meth}{\'Etablir le tableau de signes d'une expression factorisée}
    Soit $A(x)$ une expression littérale factorisée (au maximum). Pour établir son tableau de signes, il faut:
    \begin{enumerate}
        \item pour chacun des facteurs:
        \begin{enumerate}
            \item déterminer sa (ou ses) racine(s), autrement dit: pour quelle(s) valeur(s), le facteur concerné s'annule;
            \item déterminer le signe de ce facteur ailleurs qu'en les racines.
        \end{enumerate}
    \end{enumerate}
    Puis dresser le tableau:
    \begin{enumerate}[resume]
        \item sur la première ligne, lister les bornes entre lesquelles on souhaite étudier le signe de $A(x)$, ainsi que les racines précédemment trouvées, le tout en ordre croissant;
        \item prévoir une ligne de plus que le nombre de facteurs: il en faut une pour chaque facteur, ainsi que pour $A(x)$;
        \item pour chaque facteur, compléter chaque cellule créée précédemment avec le signe qui convient;
        \item on termine par le signe de $A(x)$, en appliquant les propriétés précédentes.
    \end{enumerate}
\end{meth}

\begin{ex}
On considère la fonction $f$ définie sur $[-9; 8]$ par $f(x) = (2x + 12)(- 3x + 9)$. L'expression définissant $f$ est un produit de facteurs "fonctions affines", $f$ est donc bien sous forme factorisée.
\begin{enumerate}
    \item Commençons par étudier chacun des facteurs.
    \begin{itemize}
        \item Pour le facteur $2x + 2$: on a $2x + 12 = 0$ si et seulement si $x = -\frac{12}{2} = -6$. Comme il s'agit d'une expression définissant une fonction affine de coefficient directeur positif, son signe est résumé grâce au tableau suivant:\\
        \begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $2x+12$ /1 }%
{$-\infty$ , $-6$ , $+\infty$ }%
\tkzTabLine{ , - , z , + , }
\end{tikzpicture}
\item Occupons-nous à présent de l'autre facteur: $-3x+9$. Sa racine est telle que $-3x+9 = 0^*$, soit après résolution $x = 3$. Vu comme fonction affine, on a affaire à un coefficient directeur négatif; donc le signe de ce fecteur est résumé dans le tableau suivant:\\
        \begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $-3x+9$ /1 }%
{$-\infty$ , $3$ , $+\infty$ }%
\tkzTabLine{ , + , z , - , }
\end{tikzpicture}

{\small *: la racine est appelée $x$; elle devrait être  nommée autrement, mais si elle l'était, vous pourriez être perdus!}
    \end{itemize}
    \item Nous commençons par dresser le tableau. L'énoncé dit que la fonction est définie sur $[-9;8]$; nous utiliserons $-9$ comme première borne et $8$ comme seconde borne. Nous avons aussi trouvé les racines de chacun des facteurs: $-6$ et $3$. Nous commençons donc par écrire les nombres sur la première ligne\dots
    
    \item et réservons 2+1 = 3 lignes pour la suite du tableau:\\
            \begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,$2x+12$ /1 ,$-3x+9$ /1 ,Signe $f(x)$ /1 }%
{$-9$, $-6$ , $3$ , $8$ }%
%\tkzTabLine{ , + , z , - , }
\end{tikzpicture}

    \item On "recopie" à présent les petits tableaux de signe dans le grand: on rappelle les signes, en les répétant si nécessaire. On obtient alors ceci:\\
              \begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,$2x+12$ /1 ,$-3x+9$ /1 ,Signe $f(x)$ /1 }%
{$-9$, $-6$ , $3$ , $8$ }%
\tkzTabLine{ , - , z , + ,  , +}
\tkzTabLine{ , - ,  , - , z , +}
\end{tikzpicture}

    \item Il suffit à présent de "collecter les signes": deux signes '-'; ou deux signes '+', donneront un '+'; un seul signe '-' donnera un '-'. Voici alors le tableau fini.\\
              \begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,$2x+12$ /1 ,$-3x+9$ /1 ,Signe $f(x)$ /1 }%
{$-9$, $-6$ , $3$ , $8$ }%
\tkzTabLine{ , - , z , + ,  , +}
\tkzTabLine{ , - ,  , - , z , +}
\tkzTabLine{ , - , z , + , z , -}
\end{tikzpicture}
\end{enumerate}
\end{ex}

\subsubsection{Application à la résolution d'inéquations}
\begin{meth}{Résoudre une inéquation "polynôme de degré supérieur à un"}
Pour des inéquations "complexes", voici la démarche qui doit permettre la résolution:
\begin{enumerate}
    \item Ramener l'un des membres à '0', par toute opération possible (voir méthodes précédentes).
    \item Factoriser le membre non-nul.
    \item \'Etudier le signe de cette expression factorisée.
    \item En fonction de l'inéquation obtenue en 1., déterminer les intervalles sur lesquelles elle est vérifiée.
    \item Conclure en écrivant l'ensemble de solutions de l'inéquation obtenue en 1.: c'est le même ensemble que celui des solutions de l'inéquation initiale.
\end{enumerate}
\end{meth}

\begin{ex}
Quels sont les nombres réels dont le carré est plus grand que le carré de leur double diminué de 1?\\
\'Ecrivons cela mathématiquement: nous cherchons les nombres réels $x$ tels que leur carré (soit $x^2$), soit plus grand que le carré de leur double diminué de 1; donc le carré de $2x - 1$. En d'autres termes: quels sont les réels $x$ tels que
\begin{equation*}
    x^2 > (2x-1)^2 ?
\end{equation*}
Appliquons la méthode précédente.
\begin{enumerate}
    \item Pour amener $0$ à l'un des membres, on va soustraire $(2x - 1)^2$ aux deux membres\up{*}: l'on obtient alors:
    \begin{align*}
        x^2 - (2x - 1)^2 &>0\\
    \end{align*}

    {\small *: On aurait pu choisir de soustraire $x^2$ aux deux membres, et adapter la suite en conséquence.}
    \item On doit maintenant factoriser le membre de gauche. Le développer pour espérer meilleur après est une mauvaise idée; en revanche, on doit penser aux identités remarquables. Le signe '-' au milieu des deux termes nous fait penser à la troisième d'entre elles, et ça marche: avec $a = x$ et $b = 2x - 1 $, on retrouve bien $a^2 - b^2 = x^2 - (2 x -1)^2$. Cela se factorise donc en $(a - b)(a+b) = (x - (2x-1))(x +(2x+1))$, d'où la "nouvelle" inéquation équivalente, dont on réduit ensuite les contenus de parenthèses:
    \begin{align*}
        (x - (2x-1))(x +(2x-1)) &>0\\
        (x - 2x + 1)(x + 2x - 1) &>0\\
        (-x + 1)(3x - 1) &>0
    \end{align*}
    \item Passons à l'étude de signe du membre de gauche (faites les détails -- en utilisant la méthode précédente -- pour vous convaincre que votre prof n'écrit pas n'importe quoi!). Le premier facteur s'annule pour $x = 1$, est positif avant et négatif après. Le second facteur s'annule pour $x = \frac 1 3$, est négatif avant et positif après. On dresse alors le tableau de signes:\\
                  \begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,$-x+1$ /1 ,$3x-1$ /1 , $(-x + 1)(3x - 1)$ /1, Inéq: $>0$?/1 }%
{$-\infty$, $\frac 1 3$ , $1$ , $+\infty$ }%
\tkzTabLine{ , + ,  , + , z , -}
\tkzTabLine{ , - , z , + ,  , +}
\tkzTabLine{ , - , z , + , z , -}
\node at (3.25,-4.5) {$\times$};
\node at (4,-4.5) {$\times$};
\node at (4.75,-4.5) {v};
\node at (5.5,-4.5) {$\times$};
\node at (6.25,-4.5) {$\times$};
\end{tikzpicture}

\item On observe (avec la dernière ligne du tableau précédent si besoin) que l'inéquation est vérifiée entre $\frac 1 3$ et $1$, ces deux valeurs exclues.

\item On conclut: l'ensemble des solutions de notre problème est $S = ]\frac 1 3; 1[$.
\end{enumerate}
\end{ex}
\end{comment}
