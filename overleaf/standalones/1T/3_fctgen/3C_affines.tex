
\documentclass[a4paper,twocolumn, landscape]{article}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc} 
\usepackage[french]{babel}
\usepackage{amsmath,amssymb}
\usepackage{geometry}
\geometry{top=1cm, bottom=1cm, left=1cm, right=1cm}
\setlength\columnsep{2cm}
\usepackage{tikz, tkz-tab}
\usepackage{fourier}
\usepackage{comment}
\usepackage{codesnippet}

\newcommand\defin{\paragraph{Définition.}}
\newcommand\thm[1][.]{\paragraph{Théorème \emph{#1}}}
\newcommand\ex[1][.]{\paragraph{Exemple \emph{#1}}}
\newcommand\rem{\paragraph{Remarque.}}
\newcommand\notat[1][.]{\paragraph{Notation \emph{#1}}}
\newcommand\meth[1]{\paragraph{Méthode:~\emph{#1.}}}
\newcommand\prop[1][.]{\paragraph{Propriété \emph{#1}}}
\newcommand\demo{\paragraph{Démonstration.}}
\newcommand\demoroc{\paragraph{Démonstration ($\Delta$ROC).}}

\renewcommand\thesection{\Roman{section}.}
\renewcommand\thesubsection{\arabic{subsection})}
\renewcommand\thesubsubsection{\alph{subsubsection}}

\pagestyle{empty}

\newcount\nbcopy
\nbcopy=1

\begin{document}

\begin{DefineCodeSnippet}{contents}

\setcounter{section}{2}
\section{Fonctions affines et droites}\label{sec_affines}
Les premières fonctions que l'on étudie de manière analytique sont les fonctions linéaires et les fonctions affines. Faisons le point sur ces fonctions.

\subsection{Qu'est-ce qu'une fonction affine?}
\begin{defin}
On dit d'une fonction $f$ qu'elle est \emph{affine} s' il existe deux nombres réels, notés $m$ et $p$, tels que $f$ admette pour expression algébrique $f(x) = mx+p$.
\end{defin}

\begin{rem}
Si une droite est la représentation graphique d'une fonction, elle est nécessairement non verticale. En effet, pour une droite verticale, tous les points ont la même abscisse (disons $x_0$), et donc nous ne pouvons pas associer une ordonnée unique pour l'abscisse $x_0$: elle ne définit pas de fonction.
\end{rem}

\begin{prop}
Soit $f$ une fonction affine. Alors sa représentation graphique est une droite.\\
Réciproquement, toute fonction $f$ dont la représentation graphique est une droite, est une fonction affine.
\end{prop}


\subsection{Coefficient directeur, ordonnée à l'origine}
%\subsubsection{Définitions}

\begin{defin}
Soit $f$ une fonction affine; on l'écrit $f(x) = mx+p$.\\
Le nombre $m$ s'appelle \emph{coefficient directeur} de la fonction affine (ou de la droite qui la représente).\\
Le nombre $p$ s'appelle \emph{ordonnée à l'origine} de la fonction affine (ou de la droite qui la représente).
\end{defin}


%\subsubsection{Déterminer un coefficient directeur, une ordonnée à l'origine}

\begin{prop}
Soit $f$ une fonction affine, que l'on écrit $f(x) = mx+p$, de représentation graphique $d$. Si $d$ passe par les points distincts $A:(x_A; y_A)$ et $B:(x_B; y_B)$, alors
\begin{equation*}
    m = \frac {y_B - y_A}{x_B - x_A} \qquad\text{et }p = y_A - mx_A.
\end{equation*}
\end{prop}


\begin{ex}
    Supposons qu'une fonction affine $h$ admet une représentation graphique passant par les points $A:(3; 6)$ et $B:(5;2)$. Alors:
    \begin{equation*}
        m = \frac{2-6}{5-3} = -\frac 4 3 \qquad\text{et }p = 6 - \left(-\frac 4 3\right) 3 = 6+4 = 10.
    \end{equation*}
    Donc $h$ a pour expression $h(x) = -\dfrac 4 3 x + 10$.
\end{ex}

\subsection{Propriétés des fonctions affines}
\subsubsection{Racine}


\begin{prop}
Soit $f$ une fonction affine, telle que $f(x) = mx+p$. Si $m \neq 0$, alors $f$ n'admet qu'une seule racine, le nombre $x_0 = - \dfrac p m$.
\end{prop}

\begin{demo}
Une simple résolution d'équation du premier degré:
\[
f(x) = 0\iff
mx+p=0\iff
mx=-p\iff
x=-\frac{p}{m}
\]
\end{demo}

\subsubsection{Sens de variation d'une fonction affine, représentation graphique}

\begin{prop}
Soit $f$ une fonction affine définie sur $\mathbb R$ par $f(x)=mx+p$, où $m$ et $p$ sont des réels fixés. On dit que:
\begin{itemize}
    \item $f$ est (strictement) croissante sur $\mathbb R$ quand $m > 0$;
    \item $f$ est strictement décroissante sur $\mathbb R$ quand $m < 0$;
    \item $f$ est constante sur $\mathbb R$ quand $m=0$.
\end{itemize}
\end{prop}

Ceci se résume avec la représentation graphique que nous appelons $d$.
\medskip

\noindent\begin{minipage}{0.45\linewidth}
Si $m>0$, la droite $d$ est montante. 

\definecolor{ffqqqq}{rgb}{1.,0.,0.}
\definecolor{xdxdff}{rgb}{0.49019607843137253,0.49019607843137253,1.}
\begin{tikzpicture}
\draw[->,black] (-0.5,0.) -- (4.,0.);
\draw[->,color=black] (0.,-2) -- (0.,1.5);
\draw [line width=1.6pt,color=ffqqqq,domain=-0.5:4.] plot(\x,{0.5*\x-1});
\draw (2,0) +(0,-2pt) -- +(0,2pt) node[above] {$-\frac{p}{m}$};
\draw (0,-1) +(2pt,0) -- +(-2pt,0) node[left] {$p$};
\end{tikzpicture}    
\end{minipage}\hfil\begin{minipage}{0.45\linewidth}
Si $m<0$, la droite $d$ est descendante.

\definecolor{ffqqqq}{rgb}{1.,0.,0.}
\definecolor{xdxdff}{rgb}{0.49019607843137253,0.49019607843137253,1.}
\begin{tikzpicture}
\draw[->,black] (-0.5,0.) -- (4.,0.);
\draw[->,color=black] (0.,-1.5) -- (0.,2);
\draw [line width=1.6pt,color=ffqqqq,domain=-0.5:4.] plot(\x,{-0.75*\x+1.5});
\draw (2,0) +(0,-2pt) -- +(0,2pt) node[above] {$-\frac{p}{m}$};
\draw (0,1.5) +(2pt,0) -- +(-2pt,0) node[left] {$p$};
\end{tikzpicture}
\end{minipage}


Si $m = 0$, alors la droite $d$ est horizontale, à l'ordonnée $p$.
\definecolor{ffqqqq}{rgb}{1.,0.,0.}
\begin{tikzpicture}
\draw[->,black] (-0.5,0.) -- (4.,0.);
\draw[->,color=black] (0.,-0.5) -- (0.,2);
\draw[ffqqqq, line width=1.6pt] (-0.5, 1) -- (4,1);
\draw (0,1) +(2pt,0) -- +(-2pt,0) node[below left] {$p$};
\end{tikzpicture}

\subsubsection{Tableau de signes}
Soit $f$ une fonction affine, que l'on écrit $f(x) = mx+p$, avec $m$ et $p$ deux réels, $m \neq 0$.
\medskip

\noindent\begin{minipage}{0.45\linewidth}
Si $m>0$, la droite est montante; la fonction est d'abord négative puis positive.

\begin{center}
\begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$-\infty$ , $-\frac{p}{m}$ , $+\infty$ }%
\tkzTabLine{ , - , z , + , }
\end{tikzpicture}
\end{center}
\end{minipage}\hfil\begin{minipage}{0.45\linewidth}
Si $m<0$, la droite est descendante; la fonction est d'abord positive puis négative.

\begin{center}
\begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$-\infty$ , $-\frac{p}{m}$ , $+\infty$ }%
\tkzTabLine{ , + , z , - , }
\end{tikzpicture}
\end{center}
\end{minipage}    
\end{DefineCodeSnippet}

\loop
\ExecuteCodeSnippet{contents}\pagebreak
\advance \nbcopy by -1
\unless\ifnum \nbcopy<1 \repeat
\end{document}