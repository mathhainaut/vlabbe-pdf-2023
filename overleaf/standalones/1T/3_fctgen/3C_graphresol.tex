\documentclass[a4paper,twocolumn, landscape]{article}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc} 
\usepackage[french]{babel}
\usepackage{amsmath,amssymb}
\usepackage{geometry}
\geometry{top=1cm, bottom=1cm, left=1cm, right=1cm}
\setlength\columnsep{2cm}
\usepackage{tikz, tkz-tab}
\usepackage{fourier}
\usepackage{comment}
\usepackage{codesnippet}

\newcommand\defin{\paragraph{Définition.}}
\newcommand\thm[1][.]{\paragraph{Théorème \emph{#1}}}
\newcommand\ex[1][.]{\paragraph{Exemple \emph{#1}}}
\newcommand\rem{\paragraph{Remarque.}}
\newcommand\notat[1][.]{\paragraph{Notation \emph{#1}}}
\newcommand\meth[1]{\paragraph{Méthode:~\emph{#1.}}}
\newcommand\prop[1][.]{\paragraph{Propriété \emph{#1}}}
\newcommand\demo{\paragraph{Démonstration.}}
\newcommand\demoroc{\paragraph{Démonstration ($\Delta$ROC).}}

\renewcommand\thesection{\Roman{section}.}
\renewcommand\thesubsection{\arabic{subsection})}
\renewcommand\thesubsubsection{\alph{subsubsection}}

\pagestyle{empty}

\newcount\nbcopy
\nbcopy=1

\begin{document}

\begin{DefineCodeSnippet}{contents}
\hfil{\large\bfseries Résolution graphique d'équations et d'inéquations}\hfill

\textbf{Formes $f(x) = k$ ou de type $f(x) < k$.}\par

Résoudre graphiquement une équation de type $f(x) = k$ revient à trouver les antécédents de $k$.
Pour résoudre graphiquement une inéquation de type $f(x) < k$, il faut:
\begin{itemize}
    \item trouver les antécédents de $k$ par $f$;
    \item puis identifier les intervalles où l'inégalité $f(x) < k$ a lieu.
\end{itemize}
Cela fonctionne de la même manière avec les autres inégalités.

\begin{ex}
Ci-dessous, on a la représentation graphique d'une fonction $g$. Résoudre, avec la précision permise par le graphique:
\begin{itemize}
    \item l'équation $g(x) = 4$;
    \item l'inéquation $g(x) < 2$.
\end{itemize}
\begin{tikzpicture}[scale=0.8]
\def\xa{-4.2}\def\xb{4.2}
\def\ya{-1.2}\def\yb{5.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, thick] plot(\x,{5 - 6 / (\x*\x + 1)});
\end{tikzpicture}

On observe (voir constructions ci-dessous) que:
\begin{itemize}
    \item l'équation $g(x) =4$ a pour solutions approchées $-2,25$ et $2,25$;
    \item et que l'inéquation $g(x) < 2$ a pour ensemble de solutions l'intervalle $]-1;1[$.
\end{itemize}
\noindent\begin{tikzpicture}[scale=0.7]
\def\xa{-4.2}\def\xb{4.2}
\def\ya{-1.2}\def\yb{5.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, thick] plot(\x,{5 - 6 / (\x*\x + 1)});
\draw[dashed] (\xa,4) -- (\xb,4);
%Antécédents alg: sqrt 5
\draw[dashed, -> ] (-2.236, 4) -- (-2.236, 0);
\draw[dashed, -> ] (2.236, 4) -- (2.236, 0);
\end{tikzpicture}\hfill\begin{tikzpicture}[scale=0.7]
\def\xa{-4.2}\def\xb{4.2}
\def\ya{-1.2}\def\yb{5.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, thin] plot(\x,{5 - 6 / (\x*\x + 1)});
\draw[densely dotted, very thick] (\xa,2) -- (\xb, 2);
\draw[densely dotted, ->, very thick] (-1,2) -- (-1,0);
\draw[densely dotted, ->, very thick] (1,2) -- (1,0);
\draw[domain=-1:1, samples=80, very thick] plot(\x,{5 - 6 / (\x*\x + 1)});
\node at (-1,0){\LARGE ]};
\node at (1,0){\LARGE [};
\draw[densely dotted, very thick] (-1,-3pt) -- (1, -3pt);
\end{tikzpicture}
\end{ex}

\textbf{Formes $f(x) = g(x)$ ou de type $f(x) < g(x)$.}\par

Pour résoudre de telles équations ou inéquations, on utilisera cette propriété:
\begin{prop}
	Soit $f$ et $g$ deux fonctions, et $I$ un intervalle (inclus aux ensembles de définition de $f$ et de $g$). Alors, on a:
	\begin{itemize}
		\item $f(x) < g(x)$ sur $I$ si la courbe $\mathcal C_f$ est sous la courbe $\mathcal C_g$ pour les abscisses dans $I$;
		\item $f(x) > g(x)$ sur $I$ si la courbe $\mathcal C_f$ est au-dessus de la courbe $\mathcal C_g$ pour les abscisses dans $I$;
		\item $f(x) = g(x)$ pour $x \in I$ si les courbes $\mathcal C_f$ et $\mathcal C_g$ se coupent (ou se touchent) au point d'abscisse $x$.
		\end{itemize}
\end{prop}

Ainsi, pour résoudre graphiquement une équation de type $f(x) = g(x)$, on regarde les points communs des courbes. Pour résoudre graphiquement une inéquation de type $f(x) < g(x)$, il faut de plus identifier les intervalles où l'inégalité $f(x) < g(x)$ a lieu, en regardant la position relative des courbes.

\begin{ex}
Ci-dessous, on a la représentation graphique de deux fonctions $f$ et $g$, définies sur l'intervalle $[-4;4]$. Résoudre, avec la précision permise par le graphique, l'équation $f(x) = g(x)$, puis l'inéquation $f(x) \geq g(x)$.

\begin{minipage}{6cm}
    \begin{tikzpicture}[scale=0.7]
\def\xa{-4}\def\xb{4}
\def\ya{-1.2}\def\yb{6}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
%\clip (\xa,\ya) rectangle (\xb, \yb);
\draw[domain=-4:4, samples=80, thick] plot(\x,{5 - 6 / ((\x-1)*(\x-1) + 1)})node[right] {$\mathcal C_g$};
\draw[dashed] (-4,4) parabola bend (-1,-1) (4,6) node[right] {$\mathcal C_f$};
\end{tikzpicture}
\end{minipage}%
\begin{minipage}{\dimexpr\linewidth-6cm}
    En appliquant ce qui précède, on observe que $f(x) = g(x)$ lorsque $x$ vaut environ $0,65$, $1,7$, ou $3,3$.

    Ensuite, on regarde les endroits où $\mathcal C_f$ est au-dessus de $\mathcal C_g$: cela se produit entre $0,65$ et $1,7$, puis au-delà de $3,3$.

    En conclusion: l'ensemble des solutions de $f(x) = g(x)$ est $S = \{0,65; 1,7; 3,2\}$, et l'ensemble des solutions de $f(x) \geq g(x)$ est $S' = [0,65; 1,7] \cup [3,3; 4]$.
\end{minipage}

    \begin{tikzpicture}[scale=0.7]
\def\xa{-4}\def\xb{4}
\def\ya{-1.2}\def\yb{6}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
%\clip (\xa,\ya) rectangle (\xb, \yb);
\draw[domain=-4:4, samples=80, thick] plot(\x,{5 - 6 / ((\x-1)*(\x-1) + 1)})node[right] {$\mathcal C_g$};
\draw[dashed] (-4,4) parabola bend (-1,-1) (4,6) node[right] {$\mathcal C_f$};
\foreach \x/\y in {0.65/-0.2, 1.7/1.05, 3.3/4}{\draw[densely dotted,very thick,->] (\x, \y) -- (\x,0);}
\node at (0.65,0){\LARGE [};
\node at (1.7,0){\LARGE ]};
\node at (3.3,0){\LARGE [};
\node at (4,0){\LARGE ]};
\draw[densely dotted, very thick] (3.3,-3pt) -- (4, -3pt);
\draw[densely dotted, very thick] (0.65,3pt) -- (1.7, 3pt);
\end{tikzpicture}
\end{ex}


\end{DefineCodeSnippet}

\loop
\ExecuteCodeSnippet{contents}\pagebreak
\advance \nbcopy by -1
\unless\ifnum \nbcopy<1 \repeat
\end{document}