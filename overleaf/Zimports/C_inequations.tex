\section{Transformations d'inégalités, résolution d'inéquations}
Cette partie est motivée par le fait que, souvent, les problèmes ne reviennent pas à résoudre des équations (trouver les valeurs satisfaisant une égalité), mais des \emph{inéquations} (trouver les valeurs satisfaisant des \emph{inégalités}). Comme en première partie, il fait d'abord déterminer les règles qui nous permettent de les manipuler.

\subsection{Sommes et différences d'inégalités}
\begin{prop}
Soit $A$ et $B$ deux expressions littérales. Si $A < B$, alors pour toute expression littérale $C$, on a $A+C < B+C$, et $A-C < B-C$.\\
La propriété s'adapte naturellement avec les autres inégalités.
\end{prop}

Cette propriété peut même être généralisée comme suit:
\begin{prop}
    Soit $A,\ B,\ C,\ D$ quatre expressions littérales, telles que $A < B$, et $C < D$. Alors on a $A+C < B+D$.\\
    La propriété s'adapte naturellement avec les autres inégalités.
\end{prop}
\begin{demo}
On sait que $A < B$; la propriété précédente nous dit qu'avec l'expression littérale $C$, on a $A + C < B + C$.\\
On sait aussi que $C < D$; donc, en ajoutant $B$ aux deux membres, grâce à la propriété précédente, on a $B+C < B+D$.\\
Donc, on a $A + C < B + C < B + D$: d'où le résultat.
\end{demo}

La propriété que nous venons de prouver ne fonctionne pas pour comparer $A-C$ et $B-D$\dots\ voyons-le simplement avec des nombres réels pour s'en convaincre.
\begin{ex}
On prend $A = 12;\ B = 13;\ C = 5$ et $D = 8$. On a bien $12 < 13$ et $5 < 8$. Mais $A - C = 12 - 5 = 7$, et $B - D = 13 - 8 = 5$: l'ordre que l'on attendait entre $A - C$ et $B - D$ n'est pas respecté!
\end{ex}

Si l'on imagine les membres de l'inégalité comme des plateaux d'une balance, la propriété généralisée dit que, si on ajoute plus de poids du côté le plus lourd, la balance reste dans le même état. En revanche, l'exemple ci-dessus dit que, si l'on retire plus de poids du côté le plus lourd, on ne peut plus garantir l'état de la balance.

\subsection{Multiplication et division par un réel dans des inégalités}
De la propriété introductive, il en reste un bout pour les multiplications et les divisions:
\begin{prop}
Soit $A$ et $B$ deux expressions littérales. Si $A < B$, alors pour tout nombre réel $k$ \textbf{positif}, on a $kA < kB$, et $\dfrac A k < \dfrac B k$.\\
La propriété s'adapte naturellement avec les autres inégalités.
\end{prop}

Intuitivement, avec la vue d'une balance: si elle est dans un état de déséquilibre, et que l'on double (ou triple, ou \dots ), alors la balance conserve son état. Il en est de même si l'on divise les quantités présentes d'un même facteur.

En revanche, il existe une faille lorsque l'on souhaite multiplier par un nombre réel négatif\dots\ montrons-le simplement avec des nombres réels.

\begin{ex}
On sait que $-2 < 1$. Multiplions chaque membre par $-1$\dots\ Dans le membre de gauche, on obtient 2; dans le membre de droite, on obtient $-1$: l'inégalité n'est plus conservée dans le même ordre!\\
Mais on peut tout de même en tirer quelque chose, en effectuant des additions et des soustractions:
\begin{align*}
    -2 &< 1 && \text{Ajout de 2 aux deux membres}\\
    -2{\color{blue}+2} &< 1{\color{blue}+2}\\
    0 &< 3 && \text{Soustraction de 1 aux deux membres}\\
    0{\color{blue}-1}&<3{\color{blue}-1}\\
    -1 &<2 &&\text{-- autrement dit: } 2 > -1.
\end{align*}
\end{ex}
Sur l'exemple précédent, on constate que l'inégalité a été renversée par la multiplication par $-1$. Cette observation fonctionne aussi pour les expressions littérales.

\begin{prop}
Soit $A$ et $B$ deux expressions littérales. Si $A < B$, alors $-A > -B$.
\end{prop}
\begin{demo}
    On utilise la propriété de soustraction d'une même expression littérale aux deux membres, avec $C = -A$ d'abord, puis $C = -B$ ensuite:
    \begin{align*}
        A &< B\\
        A - A &< B - A\\
        0 - B &< B - A - B\\
        -B &< -A\\
        \text{soit }-A >& -B.
    \end{align*}    
\end{demo}

Cette propriété, couplée avec la précédente, donne:
\begin{prop}
Soit $A$ et $B$ deux expressions littérales. Si $A < B$, alors pour tout nombre réel $k$ \textbf{négatif}, on a $kA > kB$, et $\dfrac A k > \dfrac B k$.\\
La propriété s'adapte naturellement avec les autres inégalités.
\end{prop}

\subsection{En résumé}
\begin{prop}
Dans toute inégalité:
\begin{itemize}
    \item le sens est \emph{conservé} si l'on ajoute ou retire une même quantité aux deux membres;
    \item le sens est \emph{conservé} si l'on multiplie ou l'on divise les deux membres par un nombre réel \emph{positif};
    \item le sens est \textbf{\emph{inversé}} si l'on multiplie  ou l'on divise les deux membres par un nombre réel \textbf{\emph{négatif}}.
\end{itemize}
\end{prop}

\subsection{Application à la résolution d'inéquations}
La propriété qui précède nous donne la base pour résoudre des inéquations du premier degré. La même méthode que celle des équations du premier degré s'applique, et nous la rappelons.

\begin{meth}{Résoudre des équations et des inéquations du premier degré}
La résolution d'équations et d'inéquations du premier degré passe par différentes étapes, chaque étape devant donner une équation ou une inéquation équivalente, plus simple à résoudre. Pour connaître l'opération à effectuer à chaque étape, il est bon de se poser les quatre questions suivantes:
\begin{enumerate}
    \item Qu'est-ce que je cherche? Quelle est l'inconnue?
    \item Qu'est-ce qui m'ennuie? Autrement dit: l'inconnue est englobée dans une expression, quels en sont les autres composants?
    \item Dans l'expression, quel est la dernière opération effectuée? Quel(s) "composant(s)" doi(ven)t être traité(s) en premier?
    \item Comment se débarasser de ce(s) "composant(s)"?
\end{enumerate}
C'est la réponse à cette dernière question qui donnera l'opération à effectuer.
\end{meth}

\begin{ex}
Résolvons dans $\mathbb R$ l'inéquation $3x - 1 \leq 5$:
\begin{align*}
    3x - 1  &\leq 5 &&\text{Membre de gauche, dernière opération:}-1\\
    3x - 1 {\color{blue}+1} &\leq 5{\color{blue}+1}\\
    3x &\leq 6 &&\text{Facteur 3 gênant: division}\\
    3x {\color{blue}/3} &\leq 6{\color{blue}/3}\\
    x &\leq 2
\end{align*}
L'ensemble des solutions est alors $S = ]-\infty; 2]$.
\end{ex}


\begin{ex}
Résoudre l'inéquation $-2x + 4 > 1$, pour $x \in [-3; 5]$.
\begin{align*}
    -2x + 4 &> 1\\
    -2x &> 1 - 4 = -3\\
    x &< \frac{-3}{-2}&&\text{(l'inégalité change de sens car on a divisé par -2)}\\
    x &< 1,5.
\end{align*}
Mais le contexte nous impose aussi $x \geq -3$. Ainsi, l'ensemble des réels $x$ tels que $f(x) > 1$ est l'ensemble $[-3; 1,5[$.
\end{ex}

\subsection{Application aux encadrements d'expressions}
\begin{ex}
Un autocar pesant entre 15,9 et 16,1 tonnes à vide, peut accueillir 60 passagers, pesant chacun entre 50 et 90 kilos. Quelle mention le constructeur devra-t-il afficher à côté du PTAC (poids total autorisé en charge)?

On note les masses en kilogrammes. Si $x$ est la masse de l'autocar et $y$ la masse d'un passager, on souhaite trouver la valeur maximale de $x + 60 y$.\\
Or, on a $15\,900 \leq x \leq 16\,100$, et $50 \leq y \leq 90$. On en déduit alors:
\begin{align*}
    50 \leq y &\leq 90 &&\text{Multiplication par 60}\\
    3\,000 \leq 60 y &\leq 5\,400 &&\text{Ajouter }x\\
    x + 3\,000  \leq x + 60 y &\leq x + 5\,400 &&\text{or, }15\,900 \leq x\text{ et } x \leq 16\,100\\
    15\,900 + 3\,000\leq x + 60 y &\leq 16\,100 + 5\,400\\
    18\,900 \leq x + 60y &\leq 21\,500.
\end{align*}
On en déduit une masse maximale de 21\,500 kilos: le PTAC que le constructeur indiquera est 21\,500 kg.
\end{ex}