\section{Expressions littérales}
\subsection{Qu'est-ce qu'une expression littérale?}
\begin{defin}
  Une \emph{expression littérale} est une expression (un "calcul") dans laquelle il y a des lettres, ces lettres désignant des nombres réels.\\
  Deux expressions sont\emph{égales} lorsqu'elles donnent les mêmes résultats, lorsque l'on remplace les lettres correspondantes par les mêmes nombres.\\
  \emph{Réduire une expression}, c'est rendre l'expression sous sa forme la plus simple possible, c'est-à-dire en utilisant le moins de termes possible.
\end{defin}

\begin{ex}
	Une expression: $x^2 + 2\times x + 1 - 6\times x + 3$.\\
	Cette expression peut se réduire pour devenir: $x^2 - 4\times x + 4$.
\end{ex}

\begin{rem}
Dans une expression littérale, les signes $\times$ ne sont pas toujours indiqués; ils sont retirés lorsqu'ils sont avant des lettres ou des parenthèses.
\end{rem}

\begin{ex}
    Les deux expressions précédentes s'écrivent plus simplement\newline   $x^2 + 2 x + 1 - 6 x + 3$ et $x^2 - 4 x + 4$.
\end{ex}

\subsection{Égalités, identités, équations}

Une égalité d'expressions est constituée de deux expressions (les membres) séparées par le signe $=$. On parle d'inégalité si le signe $=$ est remplacé par un signe d'inégalité ($<$; $\leq$; $>$; $\geq$).

Lorsque les expressions sont littérales, l'égalité (ou l'inégalité) peut être vraie pour certaines valeurs attribuées aux lettres et fausse pour d'autres valeurs.

\begin{defin}
  Si une égalité entre expressions littérales est vraie pour toutes les valeurs que l'on peut attribuer aux lettres, alors on dit que les expressions sont \emph{équivalentes} ou \emph{égales} (et on parle parfois  d'\emph{identité}).\\
  Si une égalité entre expressions littérales n'est pas toujours vraie, on parle alors d'\emph{équation}. Pour les inégalités entre expressions littérales, on parle d'\emph{inéquation}.
\end{defin}

\begin{ex}
    Les expressions  $x^2 + 2 x + 1 - 6 x + 3$ et $x^2 - 4 x + 4$ sont égales; on écrit simplement  $x^2 + 2 x + 1 - 6 x + 3 = x^2 - 4 x + 4$, et cela est une identité.
\end{ex}

\begin{ex}
    Les expressions $x^2 - 4x + 4$ et $x^2 -2x$ ne sont pas égales, car elles ne donnent pas les mêmes résultats pour toute valeur de $x$. Alors, l'égalité $x^2 - 4x + 4 = x^2 - 2x$ est une équation.
\end{ex}

\begin{defin}
    Trouver \emph{toutes} les valeurs pour lesquelles l'(in)égalité est vraie, c'est \emph{résoudre} l'(in)équation. Les valeurs ainsi trouvées sont les \emph{solutions} de l'(in)équation.
\end{defin}

\begin{defin}
  Deux équations sont dites \emph{équivalentes} si elles ont le même ensemble de solutions.
\end{defin}

On peut transformer une équation en une autre qui lui est équivalente, grâce à la propriété suivante:

\begin{prop}
  Soit $A$ et $B$ deux expressions littérales égales. Alors, pour toute expression littérale $C$, on a $A+C = B+C$ et $A-C = B-C$.\\
  De même, pour tout nombre réel $c$, on a $cA = cB$.\\
  Enfin, pour tout nombre réel $c$ non-nul, on a $\dfrac A c = \dfrac B c$.
\end{prop}

\subsection{Cas des expressions du premier degré}
\begin{meth}{Résoudre des équations du premier degré}
La résolution d'équations du premier degré passe par différentes étapes, chaque étape devant donner une équation équivalente, plus simple à résoudre. Pour connaître l'opération à effectuer à chaque étape, il est bon de se poser les quatre questions suivantes:
\begin{enumerate}
    \item Qu'est-ce que je cherche? Quelle est l'inconnue?
    \item Qu'est-ce qui m'ennuie? Autrement dit: l'inconnue est englobée dans une expression, quels en sont les autres composants?
    \item Dans l'expression, quel est la dernière opération effectuée? Quel(s) "composant(s)" doi(ven)t être traité(s) en premier?
    \item Comment se débarrasser de ce(s) "composant(s)"?
\end{enumerate}
C'est la réponse à cette dernière question qui donnera l'opération à effectuer.
\end{meth}

\begin{ex}
Résolvons l'équation $7 x + 2 = 4 x + 9$.\\
{\it On cherche \dots} la valeur de $x$. {\it Ce qui ennuie \dots} c'est qu'il apparaît dans les deux membres. {\it Le "composant" à traiter en premier} est donc le terme $4x$ du membre de droite. {\it On s'en débarrasse} en soustrayant $4x$ aux deux membres.
\begin{align*}
    7 x + 2 {\color{blue} - 4 x} &= 4 x + 9  {\color{blue} - 4 x}\\
3 x + 2 &= 9 & \text{Étape suivante: se débarrasser du terme 2}\\
3 x + 2  {\color{blue} - 2} &= 9  {\color{blue} - 2}\\
3 x &= 7& \text{Se débarrasser du facteur 3}\\
\frac {3 x} 3 &= \frac 7 3\\
x &= \frac 7 3
\end{align*}
La solution de cette équation est $\frac 7 3$; on écrit que l'ensemble des solutions est $\{ \frac 7 3 \}$.
\end{ex}

\begin{meth}{Exprimer une variable en fonction des autres dans une expression littérale}
On se pose les mêmes questions que celles qui permettent de résoudre des équations.
\end{meth}

\begin{ex}
On considère l'expression $p = \rho g (h_1 - h_2)$, qui donne en physique, la différence de pression dans un liquide de masse volumique $\rho$ entre deux hauteurs $h_1$ et $h_2$ (et $g$ est la constante de gravitation). Si l'on veut exprimer $h_2$ en fonction des autres variables, on obtient le raisonnement suivant:

\begin{align*}
p &= \rho g (h_1 - h_2) & \text{Facteurs $\rho$ et $g$ à "retirer"}\\
\frac p {\rho g} &= h_1 - h_2 &\text{Terme à retirer:} h_1\\
\frac p {\rho g} - h_1 &= - h_2 &\text{Signe (donc facteur $-1$) à retirer}\\
- \left(\frac p {\rho g} - h_1 \right) &= h_2\\
h_2 &= - \frac p {\rho g} + h_1.    
\end{align*}

\end{ex}

\subsection{Propriétés de distributivité}
\begin{prop}[Distributivité simple.]
  Pour tous nombres réels $ k, a, b$, on a l'égalité $k(a+b) = ka + kb$.
\end{prop}

\begin{rem}
L'égalité précédente doit se lire dans les deux sens. En effet, elle permet:
\begin{itemize}
    \item de transformer des produits en sommes, en distribuant le facteur commun sur chacun des termes;
    \item de transformer des sommes en produits, en mettant en évidence un facteur commun.
\end{itemize}
\end{rem}

\begin{defin}
  Quand on transforme un produit en somme, on dit que l'on \emph{développe}.\\
  Lorsqu'on transforme une somme en un produit, on dit que l'on \emph{factorise}.
\end{defin}

\begin{ex}
\begin{itemize}
    \item On peut développer l'expression $3x(2 - x)$: on a $3x(2 - x) = 3x\times2 - 3x\times x = 6x - 3x^2$.
    \item On peut factoriser l'expression $ 4x^2 - 6x$: on repère $2x$ comme facteur commun, et il vient:
    \begin{equation*}
         4x^2 - 6x = 2 x \times 2 x - 2x \times 3 = 2x \times (2x - 3).
    \end{equation*}
\end{itemize}
\end{ex}

\begin{prop}[Double distributivité.]
    Pour tous nombres réels $p,\ q,\ r,\ s$, on a l'identité $(p+q)(r+s) = pr + ps + qr + qs$.
\end{prop}
\begin{demo}
    On utilise trois fois la propriété de distributivité simple. On écrit la suite des égalités avec un mot de justification pour chaque étape:
    \begin{align*}
        (p+q)(r+s) &= (p+q)r + (p+q)s &&\text{Propriété avec } k =(p+q),\ a = r,\ b=s\\
        &= r(p+q) + s(p+q)&& \text{l'ordre des facteurs dans chaque produit}\\
        &&&\text{n'a pas d'importance}\\
        &= rp + rq + sp + sq &&\text{Propriété avec } a = p,\ b=q,\text{ et } k = r \text{ ou } k=s\\
        &= pr + ps + qr + qs&&  \text{l'ordre des facteurs dans chaque produit}\\
        &&&\text{n'a pas d'importance}
    \end{align*}
\end{demo}

\subsection{Identités remarquables}
Les identités remarquables sont des cas particuliers de double distributivité. Elles permettent de développer, mais aussi de factoriser plus facilement certaines expressions.

\begin{prop}
    Soit $a$, $b$ deux nombres réels. Alors on a les trois identités suivantes, appelées \emph{identités remarquables}:
    \begin{gather*}
        (a + b) ^2 = a^2 + 2ab + b^2\\
        (a - b) ^2 = a^2 - 2ab + b^2\\
        (a+b)(a-b) = a^2 - b^2
    \end{gather*}
\end{prop}

\begin{demo}
    La première identité remarquable est la double distributivité avec $p = a,\ q = b,\ r =a,\ s=b$.\\
    La deuxième est la double distributivité avec $p = a,\ q = -b,\ r =a,\ s=-b$.\\
    La troisième est la double distributivité avec $p = a,\ q = b,\ r =a,\ s=-b$.\\
\end{demo}

\begin{ex}
Développer $(3x + 2)^2; (5x - 3)^2; (2+3x) (2-3x)$.\\
On a $(3x + 2)^2 = (3x)^2 + 2 \times 3x \times 2 + 2^2 = 9x^2 + 12x + 4$.\\
On a $(5x - 3)^2 = (5x)^2 - 2 \times 5x \times 3 + 3^2 = 25x^2 - 30x + 9$.\\
On a $(2+3x) (2-3x) = 2^2 - (3x)^2 = 4 - 9x^2$.
\end{ex}

\subsection{Équations de degré supérieur à un}
\begin{prop}[Règle du produit nul]
    Une expression sous forme de produit est nulle si et seulement si au moins l'un de ses facteurs est nul.
\end{prop}


\begin{meth}{Résoudre des équations simples de degré supérieur à un}
Pour des équations de degré supérieur à 1, voici la démarche qui doit permettre la résolution:
\begin{enumerate}
    \item Ramener l'un des membres à '0', par toute opération possible (voir méthodes précédentes).
    \item Factoriser le membre non-nul (soit par la propriété de distributivité, soit en utilisant une identité remarquable)
    \item Appliquer la règle du produit nul: obtenir autant d'équations qu'il n'y a de facteurs.
    \item Résoudre chacune de ces équations.
    \item Conclure en écrivant l'ensemble contenant toutes les solutions trouvées: par construction, cet ensemble est celui des solutions de l'équation initiale.
\end{enumerate}
\end{meth}

\begin{ex}
    Quels sont les nombres réels $x$ tels que $(x+3)^2 = x+3$?\\
    On commence la résolution, en rendant le membre de droite nul:
    \begin{align*}
(x+3)^2 &= x+3\\
(x+3)^2 -x-3&=0\\
(x+3)^2 -(x+3) &=0 &&\text{Factorisation par } (x+3)\\
(x+3)\left((x+3)-1\right)&=0\\
(x+3)(x+2)&=0&&\text{Application règle du produit nul}\\
x+3 = 0\text{ ou }&x+2 = 0\\
x = -3 \text{ou } &x = -2
    \end{align*}
On en déduit l'ensemble des solutions: $S = \{-3; -2\}$.
\end{ex}