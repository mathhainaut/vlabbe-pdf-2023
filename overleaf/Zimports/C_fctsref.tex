
\section{D'autres fonctions de référence}
\subsection{La fonction carré}
\subsubsection{Définition et quelques valeurs}

\begin{defin}
On appelle \emph{fonction carré}, la fonction $f$ définie sur $\mathbb R$ et à valeurs réelles, qui associe à tout nombre, son carré. En d'autres termes, la fonction carré est telle que:
\begin{align*}
        f: \mathbb R &\longrightarrow \mathbb R\\
        x &\longmapsto f(x) = x^2.
\end{align*}
\end{defin}

Quelques calculs d'images:
\begin{itemize}
    \item l'image de 1 par $f $ est $f(1) = 1^2 = 1 $;
    \item l'image de 2,5 par $f$ est $f(2,5) = 2,5^2 = 6,25$;
    \item l'image de -3 par $f$ est $f(-3) = (-3)^2 = 9$.
\end{itemize}

Pour rappel, le carré de tout nombre réel est positif. Exprimé avec la fonction carré (que l'on a appelé $f$ ici), cela donne: \textit{pour tout réel $x$, on a $f(x) \geq 0$.}

\subsubsection{Courbe représentative sur $[-3;3]$}
Pour obtenir la courbe représentative de $f$, on peut dresser un tableau de valeurs:
\begin{center}
\begin{tabular}{|c|*{13}{c|}}
\hline
\textbf{$x$} & $-3$ & $-2,5$& $-2$ &$-1,5$& $-1$ & $-0,5$& 0 &0,5& 1& 1,5 &2&2,5&3\tabularnewline\hline
\textbf{$f(x)$}&9&6,25&4&2,25&1&0,25&0&0,25&1&2,25&4&6,25&9\tabularnewline\hline
\end{tabular}
\end{center}

En plaçant les points ainsi trouvés et en les reliant, on obtient le graphique suivant.
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2cm,y=1cm]
\draw [gray,dotted, xstep=1cm,ystep=1cm] (-3.1,-0.5) grid (3.1,9.6);
\draw[->,black] (-3.1,0.) -- (3.1,0.);
\foreach \x in {-3,-2,-1,1,2,3}
    \draw[shift={(\x,0)},black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
\draw[->,black] (0.,-0.5) -- (0.,9.6);
\foreach \y in {1,2,3,4,5,6,7,8,9}
    \draw[shift={(0,\y)},black] (2pt,0pt) -- (-2pt,0pt) node[left] {\footnotesize $\y$};
\node[below right] at (0,0) {\footnotesize $0$};
\clip(-3.1,-0.5) rectangle (3.1,9.6);
\draw[line width=2pt,red,smooth,samples=100,domain=-3.1:3.1] plot(\x,{(\x)^2}) node[below left] {$\mathcal C_f$};
\end{tikzpicture}
\end{center}
\begin{defin}
La courbe représentative de la fonction carré (et, on le verra plus tard, de toute fonction polynôme du second degré) s'appelle une \emph{parabole}. Pour la fonction carré, on dira qu'elle est tournée vers le haut.\\
Toute parabole (représentant une fonction) admet un point particulier où elle change de sens de variation. Ce point est appelé le \emph{sommet} de la parabole.\\
De plus, toute parabole (représentant une fonction) admet un \emph{axe de symétrie}.
\end{defin}

Le sommet de la parabole représentant la fonction carré est le point de coordonnées $(0;0)$. Son axe de symétrie est la droite d'équation $x = 0$.

\subsubsection{Tableau de signes et tableau de variations}
\begin{prop}
    La fonction carré est:
    \begin{itemize}
        \item positive (ou nulle) sur $\mathbb R$, et elle n'est nulle que pour $x = 0$;
        \item décroissante sur $]-\infty; 0[$ ;
        \item et croissante sur $[0; +\infty[$.
    \end{itemize}
\end{prop}

On peut résumer les informations de ce graphique et de cette propriété en deux tableaux.
\medskip

\begin{minipage}{0.5\linewidth}
Tableau de signes:

\begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$-\infty$ , $0$ , $+\infty$ }%
\tkzTabLine{ , + , z , + , }
\end{tikzpicture}
\end{minipage}\begin{minipage}{0.5\linewidth}

Tableau de variations:

 \begin{tikzpicture}
\tkzTabInit[espcl=1.5]{$x$/1/1,Var. $f(x)$/1}%
{$-\infty$,$0$,$+\infty$}%
\tkzTabVar{+/ , -/0, +/}%
\end{tikzpicture}
\end{minipage}

\begin{demo}
    Point par point.

    \begin{itemize}
        \item Le carré d'un nombre réel est toujours positif: donc la fonction en résultant est positive.
        \item Soit $a$ et $b$ deux réels tels que $a < b \leq 0$. La différence $b^2 - a^2$ s'écrit aussi $(b-a)(b+a)$ grâce aux identités remarquables; or, comme $a < b$, le premier facteur est positif, et comme $a$ et $b$ sont tous deux négatifs, le second facteur est aussi négatif. Dès lors:
        \begin{align*}
            \text{si } a < b,&\text{ alors } b^2 - a^2 < 0,\\
            &\text{ donc } b^2 < a^2,\\
            \text{ou encore: pour $a, b \leq 0$, si } a < b, &\text{ alors } a^2 > b^2.
        \end{align*}
        La fonction carré est décroissante sur $]-\infty; 0].$
        \item Soit $a$ et $b$ deux réels tels que $0 \leq a < b$. La différence $b^2 - a^2$ s'écrit aussi $(b-a)(b+a)$ grâce aux identités remarquables; or, comme $a < b$, le premier facteur est positif, et comme $a$ et $b$ sont tous deux positif, le second facteur est positif également. Dès lors:
        \begin{align*}
            \text{si } a < b,&\text{ alors } b^2 - a^2 > 0,\\
            &\text{ donc } b^2 > a^2,\\
            \text{ou encore: pour $a, b \geq 0$, si } a < b, &\text{ alors } a^2 < b^2.
        \end{align*}
        La fonction carré est donc croissante sur $]-\infty; 0].$*
    \end{itemize}
\end{demo}

\subsubsection{Résolution d'équations et d'inéquations}
\begin{prop}[\'Equations et inéquations avec la fonction carré]
    Soit  $k$ un nombre réel.
    \begin{enumerate}
        \item L'équation $x^2 = k$:
        \begin{itemize}
            \item n'admet aucune solution, quand $k < 0$;
            \item admet 0 comme seule solution, pour $k = 0$,
            \item admet, pour $k > 0$ deux solutions, qui sont $- \sqrt k$ et $\sqrt k$.
        \end{itemize}
        \item L'ensemble des solutions de l'inéquation $x^2 < k$ est:
        \begin{itemize}
            \item l'ensemble vide $\emptyset$, quand $k \leq 0$;
            \item l'intervalle $]-\sqrt k; \sqrt k[$, quand $k > 0$.
        \end{itemize}
        \item L'ensemble des solutions de l'inéquation $x^2 > k$ est:
        \begin{itemize}
            \item l'ensemble des réels  $\mathbb R$, quand $k < 0$;
            \item la réunion d'intervalles $]-\infty, -\sqrt k[ \cup ] \sqrt k; +\infty[$, quand $k \geq 0$.
        \end{itemize}
    \end{enumerate}

    \begin{tikzpicture}[scale=0.8]
        \def\xa{-2.6}\def\xb{2.6}
\def\ya{-1.2}\def\yb{7.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, very thick] plot(\x,{\x*\x });
\draw [dashed] (\xa, -0.8) -- (\xb, -0.8) node[right] {$k < 0$};
\draw[dashed] (\xa, 3.24) -- (\xb, 3.24) node[right] {$k > 0$};
\draw[dashed, ->] (-1.8, 3.24) -- (-1.8, 0);
\draw[dashed, ->] (1.8, 3.24) -- (1.8, 0);
    \end{tikzpicture}
\end{prop}

\subsection{La fonction cube}
\subsubsection{Définition et quelques valeurs}

\begin{defin}
On appelle \emph{fonction cube}, la fonction $f$ définie sur $\mathbb R$ et à valeurs réelles, qui associe à tout nombre, son cube. En d'autres termes, la fonction cube est telle que:
\begin{align*}
        f: \mathbb R &\longrightarrow \mathbb R\\
        x &\longmapsto f(x) = x^3.
\end{align*}
\end{defin}

Quelques calculs d'images:
\begin{itemize}
    \item l'image de 1 par $f $ est $f(1) = 1^3 = 1 $;
    \item l'image de 2,5 par $f$ est $f(2,5) = 2,5^3 = 15,625$;
    \item l'image de -3 par $f$ est $f(-3) = (-3)^3 = -27$.
\end{itemize}

\subsubsection{Courbe représentative sur $[-3;3]$}
Pour obtenir la courbe représentative de $f$, on peut dresser un tableau de valeurs:
\begin{center}
\begin{tabular}{|c|*{8}{c|}}
\hline
\textbf{$x$} & $-3$ & $-2,5$& $-2$ &$-1,5$& $-1$ & $-0,5$& \tabularnewline\hline
\textbf{$f(x)$}&$-27$&$-15,625$&$-8$&$-3,375$&$-1$&$-0,125$&\tabularnewline\hline\hline
\textbf{$x$} & 0 &0,5& 1& 1,5 &2&2,5&3\tabularnewline\hline
\textbf{$f(x)$}&0&0,125&1&3,375&8&15,625&27\tabularnewline\hline
\end{tabular}
\end{center}

En plaçant les points ainsi trouvés et en les reliant, on obtient le graphique suivant.
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=2cm,y=0.2cm]
\draw [gray,dotted, xstep=1cm,ystep=1.25cm] (-3.1,-27.5) grid (3.1,27.5);
\draw[->,black] (-3.1,0.) -- (3.1,0.);
\foreach \x in {-3,-2,-1,1,2,3}
    \draw[shift={(\x,0)},black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
\draw[->,black] (0.,-27.5) -- (0.,27.5);
\foreach \y in {-25,-20,...,25}
    \draw[shift={(0,\y)},black] (2pt,0pt) -- (-2pt,0pt) node[left] {\footnotesize $\y$};
\node[below right] at (0,0) {\footnotesize $0$};
\clip(-3.1,-28) rectangle (3.1,29);
\draw[line width=2pt,red,smooth,samples=100,domain=-3.1:3.1] plot(\x,{(\x)^3}) node[below left] {$\mathcal C_f$};
\end{tikzpicture}
\end{center}


\subsubsection{Tableau de signes et tableau de variations}
\begin{prop}
    La fonction cube est:
    \begin{itemize}
        \item négative sur $]-\infty;0]$ et positive sur $[0; +\infty[$;
        \item croissante sur $\mathbb R$.
    \end{itemize}
\end{prop}
On peut résumer les informations de ce graphique et de cette propriété en deux tableaux.
\medskip


\begin{minipage}{0.5\linewidth}
Tableau de signes:

\begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$-\infty$ , $0$ , $+\infty$ }%
\tkzTabLine{ , - , z , + , }
\end{tikzpicture}
\end{minipage}\begin{minipage}{0.5\linewidth}

Tableau de variations:

 \begin{tikzpicture}
\tkzTabInit[espcl=2]{$x$/1/1,Var. $f(x)$/1}%
{$-\infty$,$+\infty$}%
\path (N12) + (0,4pt) coordinate (A);
\path (N21) + (0,-4pt) coordinate (B);
\draw[->](A)  -- (B);
\end{tikzpicture}\end{minipage}

\begin{demo}
    Pour le signe, c'est évident: trois facteurs de même signe donnent leur signe au produit. Pour démontrer la croissance, nous avons besoin de comparer $a^3$ et $b^3$ pour deux réels $a$ et $b$ tels que $a < b$. Pour ce faire, démontrons l'égalité suivante:
    $a^3 - b^3 = (a - b) (a^2 + ab + b^2)$. Nous étudierons ensuite le signe du produit avant de conclure.
    \begin{itemize}
        \item \emph{Une nouvelle identité remarquable}

        Développons le produit donné ci-avant:
        \begin{align*}
            (a - b) (a^2 + ab + b^2) &= a^3 + a^2 b + ab^2 - a^2b - ab^2 - b^3\\
            &= a^3 - b^3
        \end{align*}
        après simplification.

        \item \emph{Signe du produit}

        On a supposé $a < b$. Alors $a - b < 0$. Pour le second facteur, deux cas de figure se présentent:
        \begin{itemize}
            \item soit $a$ et $b$ sont de même signe; dans ce cas, chaque terme est positif, et la somme $a^2 + ab + b^2$ est positive.
            \item soit $a$ et $b$ sont de signes contraires. Dans ce cas, on écrit
            \begin{equation*}
                a^2 + ab + b^2 = a^2 + 2ab + b^2 - ab = (a+b)^2 - ab;
            \end{equation*}
            puis, le premier terme est positif comme carré, le second terme est l'opposé d'un produit de nombres de signes contraires et est donc aussi positif.
        \end{itemize}
        En conclusion: le deuxième facteur est positif, quelles que soient les valeurs de $a$ et de $b$.

        Il s'ensuit que, pour $a < b$, on a $(a - b) (a^2 + ab + b^2) < 0$, soit par le premier point, $a^3 - b^3 < 0$.

        \item \emph{Conclusion}
        
        Pour tous réels $a$ et $b$ tels que $a < b$, on a  $a^3 - b^3 < 0$, soit $a^3 < b^3$. On en déduit que la fonction cube est croissante sur $\mathbb R$.
    \end{itemize}
\end{demo}

\subsubsection{Résolution d'équations et d'inéquations}

\begin{prop}[\'Equations et inéquations avec la fonction cube]
    Soit $k$ un nombre réel.
    \begin{enumerate}
        \item L'équation $x^3 = k$ admet toujours une solution: elle est appelée \emph{racine cubique de $k$}, et est notée $\sqrt[3] k$.
        \item L'ensemble des solutions de l'inéquation $x^3 < k$ est l'intervalle $]-\infty; \sqrt[3] k[$.
        \item L'ensemble des solutions de l'inéquation $x^3 > k$ est l'intervalle $] \sqrt[3] k; +\infty[$.
    \end{enumerate}
        \begin{tikzpicture}[yscale=0.25]
        \def\xa{-2.6}\def\xb{2.6}
\def\ya{-18.2}\def\yb{18.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, very thick] plot(\x,{\x*\x*\x });
\draw [dashed] (\xa, -5.832) -- (\xb, -5.832) node[right] {$k < 0$};
\draw[dashed] (\xa, 4.096) -- (\xb, 4.096) node[right] {$k > 0$};
\draw[dashed, ->] (-1.8,-5.832) -- (-1.8,0);
\draw[dashed, ->] (1.6, 4.096) -- (1.6, 0);
    \end{tikzpicture}
\end{prop}
\subsection{La fonction inverse}
\subsubsection{Définition et quelques valeurs}

\begin{defin}
On appelle \emph{fonction inverse}, la fonction $f$ définie sur $]-\infty; 0[ \cup ]0;+\infty[$ et à valeurs réelles, qui associe à tout nombre, son inverse. En d'autres termes, la fonction inverse est telle que:
\begin{align*}
        f: \mathbb R &\longrightarrow \mathbb R\\
        x &\longmapsto f(x) = \frac 1 x.
\end{align*}
\end{defin}

Quelques calculs d'images:
\begin{itemize}
    \item l'image de 1 par $f $ est $f(1) = \dfrac 1 1 = 1 $;
    \item l'image de 2,5 par $f$ est $f(2,5) = \dfrac 1 {2,5} = 0,4$;
    \item l'image de -3 par $f$ est $f(-3) = \dfrac 1 {-3} =  - \dfrac 1 3$.
\end{itemize}
Ce dernier nombre n'admet pas de forme décimale.

\subsubsection{Courbe représentative sur $[-5;5]$}
Pour obtenir la courbe représentative de $f$, on peut dresser un tableau de valeurs:
\begin{center}
\begin{tabular}{|c|*{8}{c|}}
\hline
\textbf{$x$} & $-5$ & $-4$& $-3$ &$-2$& $-1$ & $-0,5$& $-0,25$\tabularnewline\hline
\textbf{$f(x)$}&$-0,2$&$-0,25$&$-\frac 1 3$&$-0,5$&$-1$&$-2$&$-4$\tabularnewline\hline\hline
\textbf{$x$} & 0,25 &0,5& 1& 2 &3&4&5\tabularnewline\hline
\textbf{$f(x)$}&4&2&1&0,5&$\frac 1 3$&0,25&0,2\tabularnewline\hline
\end{tabular}
\end{center}

En plaçant les points ainsi trouvés et en les reliant, on obtient le graphique suivant.
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.2cm,y=1.2cm]
\draw [gray,dotted, xstep=1.2cm,ystep=1.2cm] (-5.1,-5.1) grid (5.1,5.1);
\draw[->,black] (-5.1,0.) -- (5.1,0.);
\draw[->,black] (0.,-5.1) -- (0.,5.1);
\foreach \x in {-5,-4,-3,-2,-1,1,2,3,4,5} {%
    \draw[shift={(\x,0)},black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
    \draw[shift={(0,\x)},black] (2pt,0pt) -- (-2pt,0pt) node[left] {\footnotesize $\x$};}
\node[below right] at (0,0) {\footnotesize $0$};
%\clip(-3.1,-0.5) rectangle (3.1,9.6);
\draw[line width=2pt,red,smooth,samples=100,domain=-5.1:-0.19] plot(\x,{1/\x}) ;
\draw[line width=2pt,red,smooth,samples=100,domain=0.19:5.1] plot(\x,{1/\x}) node[above left] {$\mathcal C_f$};
\end{tikzpicture}
\end{center}


\subsubsection{Tableau de signes et tableau de variations}
\begin{prop}
    La fonction inverse est:
    \begin{itemize}
        \item positive sur $]0; +\infty[$ et négative sur $]-\infty; 0[$;
         \item décroissante sur $]0; +\infty[$ et décroissante sur $]-\infty; 0[$;
    \end{itemize}
\end{prop}
On peut résumer les informations de ce graphique en deux tableaux.
\medskip

\begin{minipage}{0.5\linewidth}
Tableau de signes:

\begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$-\infty$ , $0$ , $+\infty$ }%
\tkzTabLine{ , - , d , + , }
\end{tikzpicture}
\end{minipage}\begin{minipage}{0.5\linewidth}

Tableau de variations:

 \begin{tikzpicture}
\tkzTabInit[espcl=1.5]{$x$/1/1,Var. $f(x)$/1}%
{$-\infty$,0,$+\infty$}%
\path (N11) + (0,-4pt) coordinate (A);
\path (N22) + (-8pt,4pt) coordinate (B);
\draw[->](A)  -- (B);
\path (N21) + (8pt,-4pt) coordinate (C);
\path (N32) + (0,4pt) coordinate (D);
\draw[->](C)  -- (D);
\draw [] (N21) + (-0.5pt,0) |- (N22);
\draw [] (N21) + (0.5pt,0) |- (N22) ;
\end{tikzpicture}
\end{minipage}

\begin{rem}
    Il est faux de dire que la fonction inverse est décroissante sur tout son domaine. En effet, $\dfrac 1 {-1} < \dfrac 1 1$ alors que $-1 < 1$, par exemple. Il faut envisager la fonction inverse comme définie sur deux intervalles distincts.
\end{rem}

\begin{demo}
    Pour le signe: cela se déduit du signe d'un quotient. \'Etudions les variations sur chacun des intervalles.
    \begin{itemize}
        \item \emph{Une première égalité}

        Soit $a$ et $b$ deux réels quelconques. Alors
        \begin{equation*}
            \frac 1 a - \frac 1 b = \frac b {ab} - \frac a {ab} = \frac {b-a}{ab}.
        \end{equation*}
        
        \item \emph{Sur les réels négatifs}
        
        On suppose à présent $a < b < 0$. Grâce à l'égalité précédente, on étudie le signe du quotient $\frac {b-a}{ab}$. Le numérateur est positif, le numérateur est positif, on conclut donc que:
        \begin{equation*}
            \text{ pour $a, b$ négatifs, si } a <b,\text { alors } \frac 1 a > \frac 1 b.
        \end{equation*}
        La fonction inverse est décroissante sur $]-\infty; 0[$.
            \item \emph{Sur les réels positifs}
            
        On suppose à présent $0< a < b$. La même étude de signe montre que:
        \begin{equation*}
            \text{ pour $a, b$ positifs, si } a <b,\text { alors } \frac 1 a > \frac 1 b.
        \end{equation*}
        La fonction inverse est décroissante sur $]0;+\infty[$.
    \end{itemize}
\end{demo}

\subsubsection{Résolution d'équations et d'inéquations}

\begin{prop}[\'Equations et inéquations avec la fonction inverse]
    Soit $k$ un nombre réel.
    \begin{enumerate}
        \item L'équation $\dfrac 1 x = k$ :
        \begin{itemize}
            \item n'admet pas de solution quand $k = 0$;
            \item admet exactement une solution quand $k \neq 0$, et elle est donnée par $\dfrac 1 k$.
        \end{itemize}
        \item L'ensemble des solutions de l'inéquation $\dfrac 1 x < k$ est:
        \begin{itemize}
            \item l'intervalle $]-\infty; \dfrac 1 k[$ quand $k < 0$;
            \item l'intervalle $]-\infty; 0[$ quand $k = 0$;
            \item la réunion d'intervalles $]-\infty, 0[ \cup ] \dfrac 1 k; +\infty[$, quand $k > 0$.
        \end{itemize}
       \item L'ensemble des solutions de l'inéquation $\dfrac 1 x > k$ est:
        \begin{itemize}
            \item l'intervalle $]0; \dfrac 1 k[$ quand $k > 0$;
            \item l'intervalle $]0;+\infty[$ quand $k = 0$;
            \item la réunion d'intervalles $]-\infty, \dfrac 1 k[ \cup ] 0; +\infty[$, quand $k < 0$.
        \end{itemize}
    \end{enumerate}
            \begin{tikzpicture}[scale=0.8]
        \def\xa{-5.2}\def\xb{5.2}
\def\ya{-5.2}\def\yb{5.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:-0.19, samples=80, very thick] plot(\x,{1/\x });\draw[domain=0.19:\xb, samples=80, very thick] plot(\x,{1/\x });
\draw [dashed] (\xa, -3.6) -- (\xb, -3.6) node[right] {$k < 0$};
\draw[dashed] (\xa, 0.2778) -- (\xb, 0.2778) node[right] {$k > 0$};
\draw[dashed, ->] (-0.278,-3.6) -- (-0.278,0);
\draw[dashed, ->] (3.6, 0.2778) -- (3.6, 0);
    \end{tikzpicture}
\end{prop}

\subsection{La fonction racine carrée}
\subsubsection{Définition et quelques valeurs}

\begin{defin}
On appelle \emph{fonction racine carrée}, la fonction $f$ définie sur $[0;+\infty[$ et à valeurs réelles, qui associe à tout nombre, sa racine carrée. En d'autres termes, la fonction racine carrée est telle que:
\begin{align*}
        f: \mathbb R &\longrightarrow \mathbb R\\
        x &\longmapsto f(x) = \sqrt x.
\end{align*}
\end{defin}

Quelques calculs d'images:
\begin{itemize}
    \item l'image de 1 par $f $ est $f(1) = \sqrt 1 = 1 $;
    \item l'image de 2,56 par $f$ est $f(2,56) = \sqrt{2,56} = 1,6$;
    \item l'image de -3 par $f$ n'existe pas.
\end{itemize}

\subsubsection{Courbe représentative sur $[0;10]$}
Pour obtenir la courbe représentative de $f$, on peut dresser un tableau de valeurs (approchées au centième):
\begin{center}
\begin{tabular}{|c|*{8}{c|}}
\hline
\textbf{$x$} & $0$ & $0,5$& $1$ &$1,5$& $2$ & $2,5$&$3$\tabularnewline\hline
\textbf{$f(x)$}&$0$&$0,71$&$1$&$1,22$&$1,41$&$1,58$&$1,73$\tabularnewline\hline\hline
\textbf{$x$} &$4$& 5 &6& 7& 8 &9&10\tabularnewline\hline
\textbf{$f(x)$}&2&2,24&2,45&2,65&2,82&3&3,16\tabularnewline\hline
\end{tabular}
\end{center}

En plaçant les points ainsi trouvés et en les reliant, on obtient le graphique suivant.
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.2cm,y=2cm]
\draw [gray,dotted, xstep=1.2cm,ystep=2cm] (-0.1,-0.1) grid (10.1,3.6);
\draw[->,black] (-0.1,0.) -- (10.1,0.);
\draw[->,black] (0.,-0.1) -- (0.,3.6);
\foreach \x in {1,2,...,10} {%
    \draw[shift={(\x,0)},black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
    \ifnum\x<4{\draw[shift={(0,\x)},black] (2pt,0pt) -- (-2pt,0pt) node[left] {\footnotesize $\x$};}\fi}
\node[below right] at (0,0) {\footnotesize $0$};
%\clip(-3.1,-0.5) rectangle (3.1,9.6);
\draw[line width=2pt,red,smooth,samples=100,domain=0:10.1] plot(\x,{sqrt(\x)}) node[above left] {$\mathcal C_f$};
\end{tikzpicture}
\end{center}


\subsubsection{Tableau de signes et tableau de variations}

\begin{prop}
    La fonction racine carrée est positive et croissante sur $[0; +\infty[$.
\end{prop}
On peut résumer les informations de ce graphique et de cette propriété en deux tableaux.
\medskip

\begin{minipage}{0.5\linewidth}
Tableau de signes:

\begin{tikzpicture}
\tkzTabInit[espcl=3]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$0$ , $+\infty$ }%
\tkzTabLine{ , + , }
\end{tikzpicture}
\end{minipage}\begin{minipage}{0.5\linewidth}

Tableau de variations:

 \begin{tikzpicture}
\tkzTabInit[espcl=3]{$x$/1/1,Var. $f(x)$/1}%
{0,$+\infty$}%
\path (N12) + (0,4pt) coordinate (A);
\path (N21) + (-8pt,-4pt) coordinate (B);
\draw[->](A)  -- (B);
\end{tikzpicture}
\end{minipage}

\begin{demo}
    La fonction racine carrée est positive par définition. Pour la croissance, remarquons que, si $0 \leq a < b$, alors $a - b = (\sqrt a - \sqrt b) (\sqrt a + \sqrt b)$, si bien que
    \begin{equation*}
        \sqrt a - \sqrt b = \frac {a - b}{\sqrt a + \sqrt b}.
    \end{equation*}
    Le dénominateur de ce quotient est positif, le numérateur est négatif puisque $a < b$; on en déduit donc $\sqrt a - \sqrt b < 0$, soit en conclusion:
    \begin{equation*}
        \text{si } a < b,\text{ alors }\sqrt a < \sqrt b.
    \end{equation*}
    La fonction racine carrée est donc croissante sur $[0; +\infty[$.
\end{demo}

\subsubsection{Résolution d'équations et d'inéquations}

\begin{prop}[\'Equations et inéquations avec la fonction racine carrée]
    Soit $k$ un nombre réel.
    \begin{enumerate}
        \item L'équation $\sqrt x = k$ :
        \begin{itemize}
            \item n'admet aucune solution quand $k < 0$;
            \item admet $k^2$ comme unique solution, quand $k \geq 0$.
        \end{itemize}
        \item L'ensemble des solutions de l'inéquation $\sqrt x < k$ est:
        \begin{itemize}
            \item l'ensemble vide $\emptyset $ quand $k \leq 0$;
            \item l'intervalle $[0; k^2[$, quand $k > 0$.
        \end{itemize}
         \item L'ensemble des solutions de l'inéquation $\sqrt x > k$ est:
        \begin{itemize}
            \item l'intervalle $[0; +\infty[$  quand $k < 0$;
            \item l'intervalle $[k^2; +\infty[$, quand $k \geq 0$.
        \end{itemize}
    \end{enumerate}
            \begin{tikzpicture}
        \def\xa{-0.1}\def\xb{10.1}
\def\ya{-0.4}\def\yb{3.3}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=0:\xb, samples=80, very thick] plot(\x,{sqrt(\x)});
\draw [dashed] (\xa, -0.3) -- (\xb, -0.3) node[right] {$k < 0$};
\draw[dashed] (\xa, 2.35) -- (\xb, 2.35) node[right] {$k > 0$};
\draw[dashed, ->] (5.5225, 2.35) -- (5.5225, 0);
    \end{tikzpicture}
\end{prop}