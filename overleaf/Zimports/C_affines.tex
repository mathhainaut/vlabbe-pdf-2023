
%section: fonctions affines
\section{Fonctions affines et droites}\label{sec_affines}
Les premières fonctions que l'on étudie de manière analytique sont les fonctions linéaires et les fonctions affines. Faisons le point sur ces fonctions.

\subsection{Qu'est-ce qu'une fonction affine?}
\begin{defin}
On dit d'une fonction $f$ qu'elle est \emph{affine} si sa représentation graphique est une droite.
\end{defin}

\begin{rem}
Si une droite est la représentation graphique d'une fonction, elle est nécessairement non verticale. En effet, pour une droite verticale, tous les points ont la même abscisse (disons $x_0$), et donc nous ne pouvons pas associer une ordonnée unique pour l'abscisse $x_0$: elle ne définit pas de fonction.
\end{rem}

\begin{prop}
Soit $f$ une fonction affine. Alors il existe deux nombres réels, uniques, notés $m$ et $p$, tels que $f$ admette pour expression algébrique $f(x) = mx+p$.\\
Réciproquement, toute fonction $f$ qui peut s'écrire sous la forme $f(x) = mx+p$ pour $m$ et $p$ nombres réels, est une fonction affine.
\end{prop}

\begin{comment}
\begin{demo}\emph{Une fonction affine s'écrit $f(x)=mx+p$.}

\begin{minipage}{\dimexpr\linewidth-6cm-1em\relax}
Soit $d$ la droite représentant la fonction $f$. Puisqu'elle n'est pas verticale, elle coupe l'axe des ordonnées en un point que nous nommons $P$. Comme $P$ est sur l'axe des ordonnées, son abscisse est $0$, et son ordonnée est le nombre qui convient pour $p$.\\
Sur $d$ toujours, on choisit le point $Q$ d'abscisse $1$. Son ordonnée est un certain nombre $q$, et l'on pose $m = q-p$.

Nous affirmons alors qu'une expression pour $f$ est $f(x) = mx+p$. Pour cela, choisissons un point $M$ de $d$, et vérifions que son ordonnée est bien l'image par $f$ de son abscisse, en d'autres termes: si $M:(x_M, y_M)$, a-t-on bien $y_M = mx_M +p$?
\end{minipage}\hfill
\begin{minipage}{6cm}
    \begin{tikzpicture}[scale=0.8]
\draw[->] (-3,0) -- (3,0);
\draw[->] (0,-3) -- (0,5);
\draw[very thick] (-2,-3) -- (2, 5) node[right] {$d$};
\node[left] at (0,1){$P$};
\node(Q)[right] at (0.5, 2){$Q$};
\node(M)[right] at (1.5, 4){$M$};
\node(MM)[below right] at ( 0, 4){$M'$};
\node(QQ)[above right] at (0,2) {$Q'$};
\draw[dotted] (0, 2) node[left]{$q$} -- (0.5, 2) -- (0.5, 0) node[below]{$1$}; 
\draw[dotted](0, 4) node[left]{$y_m$} -- (1.5, 4) -- (1.5, 0) node[below]{$x_M$}; 
\end{tikzpicture}
\end{minipage}


Les points $P$, $Q$ et $M$ sont alignés. Les droites horizontales d'ordonnées respectives $q$ et $y_M$ sont parallèles. Notons $Q':(0;q)$ et $M':(0,y_M)$. On peut appliquer le théorème de Thalès avec pour droites sécantes l'axe des ordonnées et $d$, et pour parallèles les droites précédentes, et obtenir les égalités:
\begin{equation*}
    \frac{PM}{PQ} = \frac{PM'}{PQ'} = \frac{MM'}{QQ'};
\end{equation*}
grâce aux coordonnées des points, la seconde égalité devient:
\begin{equation*}
    \frac{y_M - p}{q - p} = \frac{x_m}{1};
\end{equation*}
soit (produit en croix): $y_M - p = x_M(q-p)$. En se souvenant que $q-p = m$, et en ajoutant $p$ aux deux membres, on a l'égalité souhaitée.

\emph{Unicité des nombres $m$ et $p$.} Si maintenant il existait d'autres nombres $m'$ et $p'$ tels que $f(x) = m'x + p'$, alors la fonction $g$ définie par $g(x) = mx+p  - (m' x + p')$ ne pourrait qu'être la fonction qui renvoie $0$ pour toute valeur de $x$. Alors, de $g(0)$, on a $p-p' = 0$ soit $p' = p$, puis de $g(1)$, on a $m+p-(m'+p) = m - m' = 0$, soit $m' = m$.

\emph{Une fonction de type $f(x) = mx+p$ est affine.} Démontrons que deux points quelconques de la courbe représentant $f$ sont alignés avec le point $P$ de coordonnées $(0;p)$. Soit $M_1$ et $M_2$ les points de la courbe d'abscisses $x_1$ et $x_2$, deux réels quelconques. Les ordonnées respectives de ces points sont $mx_1+p$ et $mx_2+p$. Soit $N_1$ et $N_2$ les points de coordonnées respectives $(x_1;p)$ et $(x_2; p)$. Alors, on a:
\newcommand\abs[1]{|#1|}
\begin{align*}
    PN_1 &= \abs{x_1} &   PN_2 &= \abs{x_2}\\
    M_1N_1 &= \abs{mx_1} & M_2N_2 &= \abs{mx_2}\\
    M_1P &= \sqrt{x_1^2 + (mx_1)^2} & M_2P &= \sqrt{x_2^2 + (mx_2)^2}\\
    &= \sqrt{x_1^2(1 + m^2)} &   &= \sqrt{x_2^2(1+ m^2)}\\
    &= \abs{x_1}\sqrt{1 + m^2} &   &= \abs{x_2}\sqrt{1+ m^2}
\end{align*}
si bien que les triangles $PN_1M_1$ et $PN_2M_2$ ont des longueurs proportionnelles, et sont donc semblables. Les points $P,\ N_1$ et $N_2$ sont alignés, car ils sont tous sur la droite horizontale des points d'ordonnée $p$. Les angles de sommet $P$ dans les deux triangles précédents sont donc soit les mêmes, soit opposés par le sommet. Il s'ensuit que les points $P$,\ $M_1$ et $M_2$ sont alignés.

\end{demo}
\end{comment}


\subsection{Coefficient directeur, ordonnée à l'origine}
%\subsubsection{Définitions}

\begin{defin}
Soit $f$ une fonction affine; on l'écrit $f(x) = mx+p$.\\
Le nombre $m$ s'appelle \emph{coefficient directeur} de la fonction affine (ou de la droite qui la représente).\\
Le nombre $p$ s'appelle \emph{ordonnée à l'origine} de la fonction affine (ou de la droite qui la représente).
\end{defin}

\begin{rem}
Les noms donnés à ces nombres sont sensés: en effet, le coefficient directeur donne la variation en ordonnée lorsque l'abscisse augmente de 1; il donne donc une indication sur la direction de la droite. Quant à l'ordonnée à l'origine, c'est l'image de 0 par la fonction; c'est aussi l'ordonnée du point d'intersection avec la droite représentant $f$ avec l'axe des ordonnées.
\end{rem}

Comme indiqué précédemment, le coefficient directeur donne une indication sur la direction de la droite. Si deux droites ont le même coefficient directeur, alors les déplacements en ordonnées sont les mêmes étant donné un déplacement en abscisse, et donc elles sont parallèles.

\begin{comment}
\subsubsection{Quelques remarques}
Supposons que deux droites ont des coefficients directeurs $m$ et $m'$ positifs, avec $m' > m$. Alors, la droite de coefficient directeur $m'$ "monte plus vite" que celle de coefficient directeur $m$, parce que la différence d'ordonnée est plus importante.

On peut aussi avoir des coefficients directeurs négatifs: dans ce cas, un déplacement de $1$ en abscisse conduit à une baisse en ordonnée, et il s'agit alors d'une droite qui "descend". À l'instar de ce qui a été fait avant, la droite "descendra" d'autant plus que le coefficient directeur est négatif.

Si le coefficient directeur vaut $0$, alors l'écriture d'une fonction affine se simplifie en $f(x) = p$. Pour tout réel $x$, l'image par une telle fonction vaut $p$; d'où le qualificatif de fonctions \emph{constantes}. Les fonctions constantes ont pour représentation graphique des droites horizontales.

\newcommand\myscalefactor{0.6}
\newcommand\tikzcontent{%
\begin{tikzpicture}
\pgftransformscale{\myscalefactor}
\draw[very thick, ->] (-5,0) -- (5,0);
\draw[very thick, ->] (0,-5) -- (0,5);
\draw (-2,-5) -- (3,5) node[left]{$d_1$};
\draw (-5,-3) -- (5,1) node[right]{$d_2$};
\draw (-5,-2) -- (5, -2) node[right]{$d_3$};
\draw (-5, 4) -- (5, 2) node[above right]{$d_4$};
\draw (-3,5) -- (1,-5) node[right]{$d_5$};
\end{tikzpicture}}

%\newsavebox\mybox
%\savebox\mybox{%
%\mbox{\tikzcontent}}

%\usebox{\mybox}

%\pgfmathparse{\linewidth/\wd\mybox}
%\edef\myscalefactor{\pgfmathresult}
%\noindent\mbox{\tikzcontent}

\begin{minipage}{7cm}
   \tikzcontent 
\end{minipage}
\begin{minipage}{\dimexpr\linewidth-7cm-1em\relax}
    Les droites du graphique ci-contre ont été tracées par ordre décroissant de leurs coefficients directeurs. On notera que $d_3$ a un coefficient directeur nul, car elle est horizontale.

Si l'ordonnée à l'origine vaut $0$, alors l'écriture d'une fonction affine se simplifie en $f(x) = mx$. On qualifie de telles fonctions de \emph{linéaires}. De telles fonctions traduisent une situation de proportionnalité.

D'un point de vue graphique, les fonctions linéaires ont pour représentation des droites passant par l'origine. 
\end{minipage}
\end{comment}

%\subsubsection{Déterminer un coefficient directeur, une ordonnée à l'origine}

\begin{prop}
Soit $f$ une fonction affine, que l'on écrit $f(x) = mx+p$, de représentation graphique $d$. Si $d$ passe par les points distincts $A:(x_A; y_A)$ et $B:(x_B; y_B)$, alors
\begin{equation*}
    m = \frac {y_B - y_A}{x_B - x_A} \qquad\text{et }p = y_A - mx_A.
\end{equation*}
\end{prop}
\begin{demo}
    Partant des égalités $y_A = f(x_A)$ et $y_B = f(x_B)$, on a
    \begin{equation*}
        y_A = mx_A + p \qquad\text{et }y_B = mx_B+p,
    \end{equation*}
    soit
    \begin{equation*}
        p = y_A - mx_A \qquad\text{et }p = y_B - mx_B.
    \end{equation*}
    Donc on a
    \begin{align*}
        y_A - mx_A &= y_B - mx_B\\
        mx_B - mx_A &= y_B - y_A\\
        m(x_B - x_A) &= y_B - y_A\\
        m &= \dfrac{y_B - y_A}{x_B - x_A}
    \end{align*}
    (on peut écrire cette dernière égalité, car $x_B - x_A \neq 0$, puisque $A$ et $B$ sont distincts).
\end{demo}

\begin{ex}
    Supposons qu'une fonction affine $h$ admet une représentation graphique passant par les points $A:(3; 6)$ et $B:(5;2)$. Alors:
    \begin{equation*}
        m = \frac{2-6}{5-3} = -\frac 4 3 \qquad\text{et }p = 6 - \left(-\frac 4 3\right) 3 = 6+4 = 10.
    \end{equation*}
    Donc $h$ a pour expression $h(x) = -\dfrac 4 3 x + 10$.
\end{ex}

\subsection{Propriétés des fonctions affines}
\subsubsection{Racine}


\begin{prop}
Soit $f$ une fonction affine, telle que $f(x) = mx+p$. Si $m \neq 0$, alors $f$ n'admet qu'une seule racine, le nombre $x_0 = - \dfrac p m$.
\end{prop}

\begin{demo}
Une simple résolution d'équation du premier degré:
\[
f(x) = 0\iff
mx+p=0\iff
mx=-p\iff
x=-\frac{p}{m}
\]
\end{demo}

\subsubsection{Sens de variation d'une fonction affine, représentation graphique}

\begin{prop}
Soit $f$ une fonction affine définie sur $\mathbb R$ par $f(x)=mx+p$, où $m$ et $p$ sont des réels fixés. On dit que:
\begin{itemize}
    \item $f$ est (strictement) croissante sur $\mathbb R$ quand $m > 0$;
    \item $f$ est strictement décroissante sur $\mathbb R$ quand $m < 0$;
    \item $f$ est constante sur $\mathbb R$ quand $m=0$.
\end{itemize}
\end{prop}

Ceci se résume avec la représentation graphique que nous appelons $d$.
\medskip

\noindent\begin{minipage}{0.45\linewidth}
Si $m>0$, la droite $d$ est montante. 

\definecolor{ffqqqq}{rgb}{1.,0.,0.}
\definecolor{xdxdff}{rgb}{0.49019607843137253,0.49019607843137253,1.}
\begin{tikzpicture}
\draw[->,black] (-0.5,0.) -- (4.,0.);
\draw[->,color=black] (0.,-2) -- (0.,1.5);
\draw [line width=1.6pt,color=ffqqqq,domain=-0.5:4.] plot(\x,{0.5*\x-1});
\draw (2,0) +(0,-2pt) -- +(0,2pt) node[above] {$-\frac{p}{m}$};
\draw (0,-1) +(2pt,0) -- +(-2pt,0) node[left] {$p$};
\end{tikzpicture}    
\end{minipage}\hfil\begin{minipage}{0.45\linewidth}
Si $m<0$, la droite $d$ est descendante.

\definecolor{ffqqqq}{rgb}{1.,0.,0.}
\definecolor{xdxdff}{rgb}{0.49019607843137253,0.49019607843137253,1.}
\begin{tikzpicture}
\draw[->,black] (-0.5,0.) -- (4.,0.);
\draw[->,color=black] (0.,-1.5) -- (0.,2);
\draw [line width=1.6pt,color=ffqqqq,domain=-0.5:4.] plot(\x,{-0.75*\x+1.5});
\draw (2,0) +(0,-2pt) -- +(0,2pt) node[above] {$-\frac{p}{m}$};
\draw (0,1.5) +(2pt,0) -- +(-2pt,0) node[left] {$p$};
\end{tikzpicture}
\end{minipage}


Si $m = 0$, alors la droite $d$ est horizontale, à l'ordonnée $p$.
\definecolor{ffqqqq}{rgb}{1.,0.,0.}
\begin{tikzpicture}
\draw[->,black] (-0.5,0.) -- (4.,0.);
\draw[->,color=black] (0.,-0.5) -- (0.,2);
\draw[ffqqqq, line width=1.6pt] (-0.5, 1) -- (4,1);
\draw (0,1) +(2pt,0) -- +(-2pt,0) node[below left] {$p$};
\end{tikzpicture}

\subsubsection{Tableau de signes}
Soit $f$ une fonction affine, que l'on écrit $f(x) = mx+p$, avec $m$ et $p$ deux réels, $m \neq 0$.
\medskip

\noindent\begin{minipage}{0.45\linewidth}
Si $m>0$, la droite est montante; la fonction est d'abord négative puis positive.

\begin{center}
\begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$-\infty$ , $-\frac{p}{m}$ , $+\infty$ }%
\tkzTabLine{ , - , z , + , }
\end{tikzpicture}
\end{center}
\end{minipage}\hfil\begin{minipage}{0.45\linewidth}
Si $m<0$, la droite est descendante; la fonction est d'abord positive puis négative.

\begin{center}
\begin{tikzpicture}
\tkzTabInit[espcl=1.5]
{$x$ / 1 ,Signe $f(x)$ /1 }%
{$-\infty$ , $-\frac{p}{m}$ , $+\infty$ }%
\tkzTabLine{ , + , z , - , }
\end{tikzpicture}
\end{center}
\end{minipage}