\section{Généralités sur les fonctions}
\subsection{Fonction, image, antécédents}
\begin{defin}
Soit $D \subset \mathbb R$ un ensemble (intervalle ou réunion d'intervalles). Définir une fonction $f$, c'est associer à chaque nombre $x \in D$ un et un seul (autre) nombre réel, que l'on note $f(x)$.\\
L'ensemble $D$ s'appelle \emph{ensemble de définition} de $f$.
\end{defin}

\begin{defin}
Soit $f$ une fonction, $D$ son ensemble de définition, et $x \in D$. Le nombre $f(x)$ que l'on obtient est appelé \emph{image de $x$ par $f$}.
\end{defin}

\begin{ex}
Calculer l'image de:
\begin{itemize}
    \item $1$ par la fonction carré;
    \item $-2$ par la fonction cube;
    \item $9$ par la fonction racine carrée.
\end{itemize}
\end{ex}

\begin{defin}
Soit $f$ une fonction, et $b$ un nombre réel. Tout nombre $x \in D$ tel que $f(x)= b$, est appelé \emph{antécédent de $b$ par $f$}.
\end{defin}

\begin{ex}
Déterminer, si possible, tous les antécédents de:
\begin{itemize}
    \item $4$ par la fonction carré;
    \item $125$ par la fonction cube;
    \item $-1$ par la fonction racine carrée.
\end{itemize}
\end{ex}

\begin{rem}
Pour un réel $b$ fixé, une fonction peut admettre 0, 1 ou plusieurs antécédents.
\end{rem}

\subsection{Représenter une fonction}
\subsubsection{Représentation algébrique}
Une fonction peut être définie avec une expression littérale.

\begin{ex}
La fonction $f$ définie sur $\mathbb R$ par $x \mapsto x^2 - 2 x +  1$ (on écrit plus souvent $f(x) = x^2 - 2 x + 1$).

L'image de $3$ est $f(3) = 3^2 - 2 .3 + 1 = 4$.

Recherchons les antécédents de 1:\\
on résout l'équation $f(x) = 1$:
\begin{gather*}
    x^2 - 2 x + 1 = 1\\
    x^2 - 2x = 0\\
    x(x - 2) = 0\\
    x = 0 \text{ ou } x-2 = 0;
\end{gather*}
les antécédents de $1$ sont $0$ et $2$.
\end{ex}

\subsubsection{\`A l'aide d'un algorithme}
\begin{ex}
La fonction $f$ est définie à l'aide des instructions suivantes:

\noindent Choisir un nombre.\\
L'élever au carré.\\
Lui retirer son double.\\
Ajouter 1 au résultat.\\
\end{ex}

Détermination d'images: application des instructions.

Détermination d'antécédents: si possible, en "inversant" les instructions; sinon, en résolvant une équation.

\subsubsection{\`A l'aide d'une représentation graphique}
\label{recherche_antecedents}

\def\graphicsize{5cm}
\noindent\begin{minipage}{\graphicsize}
\begin{tikzpicture}[scale=0.6]
\def\xa{-3.2}\def\xb{4.2}
\def\ya{-1.2}\def\yb{12.2}
\draw[gray, very thin]  (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=-2.47:4.2, samples=80, thick] plot(\x,{(\x-1)^2)});
\draw[dashed, ->, very thick] (-2,0) -- (-2,9) -- (0,9);
\draw[densely dotted, very thick] (\xa,4) -- (\xb, 4);
\foreach \x in {-1,3} \draw [->, dotted, very thick] (\x,4) -- (\x,0);
\end{tikzpicture}
\end{minipage}
\begin{minipage}{\dimexpr\linewidth-\graphicsize\relax}

\begin{meth}{Lire graphiquement une image}

Pour lire l'image d'un nombre réel $a$ d'une fonction dont on a la courbe représentative $\mathcal C$:
\begin{enumerate}
    \item repérer le point d'abscisse $a$ sur l'axe des abscisses;
    \item tracer le segment vertical de ce point vers la courbe $\mathcal C$;
    \item depuis ce point d'intersection, tracer le segment horizontal entre $\mathcal C$ et l'axe des ordonnées.
\end{enumerate}
\end{meth}

\begin{meth}{Lire graphiquement des antécédents éventuels}

Pour lire les éventuels antécédents d'un nombre réel $k$ d'une fonction dont on a la courbe représentative $\mathcal C$:
\begin{enumerate}
    \item repérer le point d'ordonnée $k$ sur l'axe des ordonnées;
    \item tracer la droite horizontale passant par ce point;
    \item pour \textbf{chaque} intersection de cette droite avec $\mathcal C$, tracer un segment vertical entre $\mathcal C$ et l'axe des abscisses;
    \item lire toutes les abscisses ainsi trouvées.
\end{enumerate}
\end{meth}
\end{minipage}


Lecture graphique d'images :\\
exemple avec les tirets : lecture de l'image de -2.\\
on trace la droite verticale à l'abscisse -2;
on lit l'ordonnée du point d'intersection de cette droite avec la courbe qui représente la fonction (ici: 9).

Lecture graphique d'antécédents :\\
exemple avec les pointillés : lecture des antécédents de 4.\\
on trace la droite horizontale à l'ordonnée -4;
on repère tous les points de la courbe qui intersectent cette droite,
on lit (par des droites  verticales) l'abscisse de chacun de ces points.\\
Ici, on lit pour abscisses $-1$ et $3$.\\
Les antécédents de $4$ par $f$ sont $-1$ et $3$.

\subsection{Courbe représentative}
\begin{defin}
Soit $f$ une fonction. Les points de coordonnées $(x,y)$ tels que $x \in D$ et $y = f(x)$, forment la \emph{courbe représentative de $f$}.
\end{defin}

\begin{ex}
On considère la fonction $g$ définie sur $\mathbb R$ par $g(x) = x^2 - 5 x + 6$. Les points suivants sont-ils sur la courbe représentative de $g$?

A(1;2) \qquad B(10;44) \qquad C(3;0) \qquad D(-2;0)

On observe que:
\begin{itemize}
    \item $g(1) = 1^2 - 5 \times 1 + 6 = 2$, donc $A \in \mathcal C_g$;
    \item $g(10) = 10^2 - 5 \times 10 + 6 = 56 \neq 44$, donc $B \notin \mathcal C_g$;
    \item $g(3) = 3^2 - 5 \times 3 + 6 = 0$, donc $C \in \mathcal C_g$;
    \item $g(-2) = (-2)^2 - 5 \times (-2) + 6 = 0$, donc $D \in \mathcal C_g$.
\end{itemize}\end{ex}

\begin{rem}
Pour vérifier si un point de coordonnées $(x,y)$ est sur la courbe représentative d'une fonction $f$, il suffit alors de vérifier que son ordonnée est l'image de son abscisse. En d'autres termes:
\begin{equation*}
    (x; y)\text{ est sur la courbe} \Longleftrightarrow y = f(x).
\end{equation*}
\end{rem}

\subsection{Fonctions paires et impaires}
\begin{defin}
On dit qu'une fonction $f$ est paire si, pour tout $x \in D$, on a $-x \in D$ et $f(-x) = f(x)$.
\end{defin}

\begin{prop}
Soit $f$ une fonction. Alors $f$ est paire si, et seulement si, sa courbe représentative est symétrique par rapport à l'axe des ordonnées.
\end{prop}

Cela signifie que si un point de coordonnées $(x,y)$ est sur cette courbe, alors le point de coordonnées $(-x,y)$ y est aussi.

\begin{ex}
$f(x) = x^2$; $f(x) = 5 - \frac 6 {x^2 + 1}$
\end{ex}

\begin{defin}
On dit qu'une fonction $f$ est impaire si, pour tout $x \in D$, on a $-x \in D$ et $f(-x) = -f(x)$.
\end{defin}

\begin{prop}
Soit $f$ une fonction. Alors $f$ est impaire si, et seulement si, sa courbe représentative est symétrique par rapport à l'origine du repère.
\end{prop}

Cela signifie que si un point de coordonnées $(x,y)$ est sur cette courbe, alors le point de coordonnées $(-x,-y)$ y est aussi.

\begin{ex}
$f(x) = x^3$; $f(x) = 3x + \frac x {x^2 + 3}$
\end{ex}

\subsection{Sens de variation}
\begin{defin}
Soit $I$ un intervalle, et $f$ une fonction définie sur $I$. On dit que:
\begin{itemize}
    \item $f$ est croissante sur $I$ si elle conserve l'ordre des éléments de $I$; c'est-à-dire: pour tous $a$, $b$ de $I$ tels que $a\leq b$, on a $f(a) \leq f(b)$;
    \item $f$ est décroissante sur $I$ si elle inverse l'ordre des éléments de $I$; c'est-à-dire: pour tous $a$, $b$ de $I$ tels que $a\leq b$, on a $f(a) \geq f(b)$;
    \item $f$ est constante sur $I$ si l'image reste la même sur tout $I$; c'est-à-dire: pour tous $a$, $b$ de $I$, $f(a) = f(b)$.
\end{itemize}
\end{defin}

On peut aussi définir la notion de fonction strictement croissante et de fonction strictement décroissante (croissante ou décroissante, sans jamais être constante).

\begin{defin}
On dit d'une fonction qu'elle est (strictement) monotone sur un intervalle si elle y est (strictement) croissante ou (strictement) décroissante.
\end{defin}

\subsection{Résolution graphique d'équations et d'inéquations}
Résoudre graphiquement une équation de type $f(x) = k$ revient à trouver les antécédents de $k$. Voir \ref{recherche_antecedents}.

Pour résoudre graphiquement une inéquation de type $f(x) < k$, il faut:
\begin{itemize}
    \item trouver les antécédents de $k$ par $f$;
    \item puis identifier les intervalles où l'inégalité $f(x) < k$ a lieu.
\end{itemize}
Cela fonctionne de la même manière avec les autres inégalités.

\begin{ex}
Ci-dessous, on a la représentation graphique d'une fonction $g$. Résoudre, avec la précision permise par le graphique:
\begin{itemize}
    \item l'équation $g(x) = 4$;
    \item l'inéquation $g(x) < 2$.
\end{itemize}
\begin{tikzpicture}[scale=0.8]
\def\xa{-4.2}\def\xb{4.2}
\def\ya{-1.2}\def\yb{5.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, thick] plot(\x,{5 - 6 / (\x*\x + 1)});
\end{tikzpicture}

On observe (voir constructions ci-dessous) que:
\begin{itemize}
    \item l'équation $g(x) =4$ a pour solutions approchées $-2,25$ et $2,25$;
    \item et que l'inéquation $g(x) < 2$ a pour ensemble de solutions l'intervalle $]-1;1[$.
\end{itemize}
\noindent\begin{tikzpicture}[scale=0.7]
\def\xa{-4.2}\def\xb{4.2}
\def\ya{-1.2}\def\yb{5.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, thick] plot(\x,{5 - 6 / (\x*\x + 1)});
\draw[dashed] (\xa,4) -- (\xb,4);
%Antécédents alg: sqrt 5
\draw[dashed, -> ] (-2.236, 4) -- (-2.236, 0);
\draw[dashed, -> ] (2.236, 4) -- (2.236, 0);
\end{tikzpicture}\hfill\begin{tikzpicture}[scale=0.7]
\def\xa{-4.2}\def\xb{4.2}
\def\ya{-1.2}\def\yb{5.2}
\draw [gray] (\xa,\ya) grid (\xb, \yb);
\draw[->, very thick] (\xa,0) -- (\xb,0);
\draw[->, very thick] (0,\ya) -- (0,\yb);
\draw[domain=\xa:\xb, samples=80, thin] plot(\x,{5 - 6 / (\x*\x + 1)});
\draw[densely dotted, very thick] (\xa,2) -- (\xb, 2);
\draw[densely dotted, ->, very thick] (-1,2) -- (-1,0);
\draw[densely dotted, ->, very thick] (1,2) -- (1,0);
\draw[domain=-1:1, samples=80, very thick] plot(\x,{5 - 6 / (\x*\x + 1)});
\node at (-1,0){\LARGE ]};
\node at (1,0){\LARGE [};
\draw[densely dotted, very thick] (-1,-3pt) -- (1, -3pt);
\end{tikzpicture}
\end{ex}
